package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipExam;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/29/2018.
 */

public interface IclasstipExamDataSource {

    Flowable<List<classtipExam>> getExams();

    Flowable<List<classtipExam>> getExamsById(int courseId,int subId);

    int isExist(int examId);

    int countExams();

    void insertExams(classtipExam...classtipExams);
}
