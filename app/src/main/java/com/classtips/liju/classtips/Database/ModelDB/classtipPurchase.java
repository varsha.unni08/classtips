package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/15/2018.
 */
@Entity(tableName = "classtipPurchase")
public class classtipPurchase {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    public int id;

    @ColumnInfo(name="subjectName")
    public String subjectName;

    @ColumnInfo(name="price")
    public String price;

    @ColumnInfo(name="courseId")
    public int courseId;

    @ColumnInfo(name="subId")
    public int subId;

    @ColumnInfo(name="phone")
    public String phone;


}
