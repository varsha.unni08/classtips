package com.classtips.liju.classtips.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.classtips.liju.classtips.Model.Course;
import com.classtips.liju.classtips.R;

import java.util.List;

/**
 * Created by l on 24-08-2019.
 */

public class CourseSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<Course> exams;
    private final int mResource;

    public CourseSpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                         @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        exams = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView courseName = (TextView) view.findViewById(R.id.txt_course_name);
        TextView id = (TextView) view.findViewById(R.id.txt_id);
        TextView expiry = (TextView) view.findViewById(R.id.txt_expiry);

        Course examsData = exams.get(position);

        courseName.setText(examsData.course_name);
        id.setText(examsData.course_id);
        expiry.setText(examsData.expiry_date);
        //Toast.makeText(mContext, examsData.exam_id, Toast.LENGTH_SHORT).show();

        return view;
    }
}
