package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.ClassMode;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.SubjectActivity;
import com.classtips.liju.classtips.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 8/10/2018.
 */

public class ClassmodeAdapter extends RecyclerView.Adapter<ClassmodeViewHolder> {
    Context context;
    List<ClassMode> classModes;

    public ClassmodeAdapter(Context context, List<ClassMode> classModes) {
        this.context = context;
        this.classModes = classModes;
    }
    @NonNull
    @Override
    public ClassmodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.classmode_layout,null);
        return new ClassmodeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClassmodeViewHolder holder, final int position) {
        //Load image
        Picasso.with(context)
                .load(classModes.get(position).link)
                .into(holder.image_classmod);
        holder.txt_clsmod_name.setText(classModes.get(position).class_mode_name);
        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                Common.currentClassmode= classModes.get(position);
                Common.currentClasstipClassMode=null;

                context.startActivity(new Intent(context,SubjectActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return classModes.size();
    }
}
