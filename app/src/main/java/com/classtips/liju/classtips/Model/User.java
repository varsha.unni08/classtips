package com.classtips.liju.classtips.Model;

import java.util.Date;

/**
 * Created by LIJU on 8/4/2018.
 */

public class User {
    private String phone;
    private String name;
    private String address;
    private String birthdate;
    private String course;
    private Date expiry_date;
    private String imei;
    private String version;
    private String flag;
    private String expiry_flag;
    private String topic_updtn;
    private String video_updtn;
    private String question_updtn;
    private Date instln_date;
    private Date app_running_date;
    private String error_msg;

    public User() {
    }

    public String getCourse() {return course;}

    public void setCourse(String course) {
        this.course = course;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Date getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(Date expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getExpiry_flag() {
        return expiry_flag;
    }

    public void setExpiry_flag(String expiry_flag) {
        this.expiry_flag = expiry_flag;
    }

    public String getTopic_updtn() {
        return topic_updtn;
    }

    public void setTopic_updtn(String topic_updtn) {
        this.topic_updtn = topic_updtn;
    }

    public String getVideo_updtn() {
        return video_updtn;
    }

    public void setVideo_updtn(String video_updtn) {
        this.video_updtn = video_updtn;
    }

    public String getQuestion_updtn() {
        return question_updtn;
    }

    public void setQuestion_updtn(String question_updtn) {
        this.question_updtn = question_updtn;
    }

    public Date getInstln_date() {
        return instln_date;
    }

    public void setInstln_date(Date instln_date) {
        this.instln_date = instln_date;
    }

    public Date getApp_running_date() {
        return app_running_date;
    }

    public void setApp_running_date(Date app_running_date) {
        this.app_running_date = app_running_date;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
