package com.classtips.liju.classtips.Fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.VideoView;

import com.classtips.liju.classtips.Adapter.MainHomePagerAdapter;
import com.classtips.liju.classtips.ChapterActivity;
import com.classtips.liju.classtips.Database.AdapterDB.ClassModeAdapterDB;
import com.classtips.liju.classtips.Database.AdapterDB.SubjectAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;
import com.classtips.liju.classtips.Database.ModelDB.classtipExam;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;
import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.MainActivity;
import com.classtips.liju.classtips.Model.ChapterTopic;
import com.classtips.liju.classtips.Model.Exams;
import com.classtips.liju.classtips.Model.MainVideo;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.Model.QuestionYear;
import com.classtips.liju.classtips.Model.Topic;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.DownloadUtils;
import com.classtips.liju.classtips.Utils.ProgressDialog;
import com.classtips.liju.classtips.config.AppPreferences;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.classtips.liju.classtips.config.ApplicationConstants.APPLICATION_DATA;
import static com.classtips.liju.classtips.config.ApplicationConstants.BULK_DATA_STATUS;
import static com.classtips.liju.classtips.config.ApplicationConstants.PHONE;

public class HomeFragment extends Fragment {

    private Unbinder unbinder;
    IClassTipAPI mService;
    RecyclerView sub_menu, class_menu;
    ImageButton play;
    VideoView videoView;
    private ProgressDialog progressDialog;
    String phone, courseId, instlnDate, expiryDate;
    //RxJava
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    ClassModeAdapterDB classModeAdapterDB;
    SubjectAdapterDB subjectAdapterDB;
    AppPreferences appPreferences;

    ViewPager viewPager;

    TabLayout tab_layout;
    Timer   timer;

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_home, container, false);
        unbinder= ButterKnife.bind(this,view);

        mService = Common.getAPI();
        videoView = view.findViewById(R.id.videoView);
        viewPager=view.findViewById(R.id.viewPager);
        tab_layout=view.findViewById(R.id.tab_layout);


        play = view.findViewById(R.id.btn_play);
        progressDialog = new ProgressDialog(requireActivity());
        sub_menu = view.findViewById(R.id.sub_menu);
        appPreferences = AppPreferences.getInstance(requireActivity(), APPLICATION_DATA);
        sub_menu.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        sub_menu.setHasFixedSize(true);

        subjectAdapterDB = new SubjectAdapterDB(getActivity(), new ArrayList<classtipSubject>());
        sub_menu.setAdapter(subjectAdapterDB);

        class_menu = view.findViewById(R.id.class_menu);
        class_menu.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        class_menu.setHasFixedSize(true);

        classModeAdapterDB = new ClassModeAdapterDB(getActivity(), new ArrayList<classtipClassMode>());
        class_menu.setAdapter(classModeAdapterDB);



        compositeDisposable.add(Common.userRepository.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipUser>>() {
                    @Override
                    public void accept(List<classtipUser> classtipUsers) throws Exception {
                        Log.d("classtip234", new Gson().toJson(classtipUsers));
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                phone = rec.getString("phone");
                                courseId = rec.getString("courseId");
                                instlnDate = rec.getString("instlnDate");
                                expiryDate = rec.getString("expiryDate");
                            }
                            getSubjectDB(courseId);
                            getClassModeDB();
                            if (isConnectingToInternet()) {
                                if (!appPreferences.getBoolean(BULK_DATA_STATUS)){
                                    getBulkData(courseId);
                                    progressDialog.show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }));

        setupPagerData();
        return view;
    }


    private void setupPagerData()
    {
        for (int i=0;i<4;i++)
        {

            tab_layout.addTab(tab_layout.newTab());
            viewPager.setId(i);
        }

        viewPager.setAdapter(new MainHomePagerAdapter(getActivity().getSupportFragmentManager()));
        tab_layout.setupWithViewPager(viewPager);

       // viewPager.setPageTransformer(true,new ZoomOutPageTransformer());

        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(viewPager,0), 100, 5 * 1000); // delay

    }


    class RemindTask extends TimerTask {


        ViewPager viewPager;
        int page;

        public RemindTask(ViewPager viewPager,int page) {
            this.viewPager = viewPager;
            this.page=page;
        }

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.

            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 4) { // In my case the number of pages are 5
                        timer.cancel();
                        // Showing a toast for just testing purpose
//                        Toast.makeText(getApplicationContext(), "Timer stoped",
//                                Toast.LENGTH_LONG).show();
                    } else {
                        viewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }


    private void getBulkData(final String course_id) {


      final   AppPreferences appPreferences = AppPreferences.getInstance(getActivity(), APPLICATION_DATA);


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                compositeDisposable.add(mService.getSubjectswithpurchase(course_id,appPreferences.getString(PHONE))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .onExceptionResumeNext(new Observable<List<classtipSubject>>() {
                            @Override
                            protected void subscribeActual(Observer<? super List<classtipSubject>> observer) {
                                Log.d("exception123", "subjectexception");
                            }
                        })
                        .doOnError(new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                               // Toast.makeText(getActivity(), "Net Connection is too slow", Toast.LENGTH_SHORT).show();
                                if (!DownloadUtils.isConnectingToInternet(getActivity())) {
                                    DownloadUtils.showCheckConnectiondialog(getActivity());

                                } else {
                                    Toast.makeText(getActivity(), "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();

                                }


                            }
                        })
                        .doOnComplete(new Action() {
                            @Override
                            public void run() throws Exception {
//                                subjectAdapterDB.notifyDataSetChanged();

                               // progressDialog.dismiss();
                                getAllClassmodeData();
                            }
                        })
                        .subscribe(new Consumer<List<classtipSubject>>() {
                            @Override
                            public void accept(List<classtipSubject> subjects) throws Exception {
                                subjectAdapterDB.setSubjectList(subjects);
                                try {
                                    JSONArray req = new JSONArray(new Gson().toJson(subjects));
                                    if (req.length()!=Common.subjectRepository.countSubjects()){
                                        Common.subjectRepository.emptySubjects();
                                    }
                                    for (int i = 0; i < req.length(); ++i) {
                                        JSONObject rec = req.getJSONObject(i);
                                        String subId = String.valueOf(rec.getInt("subject_id"));
                                        String link = rec.getString("link");
                                        String subjectName = rec.getString("subject_name");
                                        String courseId = rec.getString("course_id");
                                        String price = rec.getString("prize");
                                        boolean ispurchased=rec.getBoolean("ispurchased");



                                        if (Common.subjectRepository.isExist(Integer.parseInt(subId)) != 1) {
                                            classtipSubject classtipSubjects = new classtipSubject();
                                            classtipSubjects.subject_id = subId;
                                            classtipSubjects.subject_name = subjectName;
                                            classtipSubjects.course_id = courseId;
                                            classtipSubjects.link = link;
                                            classtipSubjects.prize = price;
                                            classtipSubjects.ispurchased=ispurchased;
                                            Common.subjectRepository.insertSubjects(classtipSubjects);

                                            Common.subjectRepository.countSubjects();
                                            //Toast.makeText(HomeActivity.this, new Gson().toJson(Common.subjectRepository.countSubjects()), Toast.LENGTH_SHORT).show();
                                        } else {
                                            classtipSubject classtipSubjects = new classtipSubject();
                                            classtipSubjects.subject_id = subId;
                                            classtipSubjects.subject_name = subjectName;
                                            classtipSubjects.course_id = courseId;
                                            classtipSubjects.link = link;
                                            classtipSubjects.prize = price;
                                            classtipSubjects.ispurchased=ispurchased;

                                            Common.subjectRepository.updateSubjects(classtipSubjects);
                                            
                                        }
                                    }
                                } catch (JSONException | RuntimeException e) {
                                    Log.d("exception123", "subject");
                                    e.printStackTrace();
                                }
                            }
                        }));
            }
        });
    }

//    private void getAllTopicData() {
//        compositeDisposable.add(mService.getTopic(courseId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .onExceptionResumeNext(new Observable<List<Topic>>() {
//                    @Override
//                    protected void subscribeActual(Observer<? super List<Topic>> observer) {
//                        Log.d("exception123","Topicexception");
//                    }
//                })
//                .doOnComplete(new Action() {
//                    @Override
//                    public void run() throws Exception {
//
//
//                        getAllVideoData();
//
//                       // progressDialog.dismiss();
//                    }
//                })
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Toast.makeText(getActivity(), "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .subscribe(new Consumer<List<Topic>>() {
//                    @Override
//                    public void accept(List<Topic> topics) throws Exception {
//
//                        try {
//                            JSONArray req = new JSONArray(new Gson().toJson(topics));
//
//                            for (int i = 0; i < req.length(); ++i) {
//                                JSONObject rec = req.getJSONObject(i);
//                                String id = String.valueOf(rec.getInt("topic_id"));
//                                String topicName = rec.getString("topic_name");
//                                String chapId=rec.getString("chptr_id");
//                                String priority=rec.getString("priority");
//                                String topicType=rec.getString("topic_type");
//                                String classmodeId=rec.getString("classmode_id");
//                                String video_id=rec.getString("video_id");
//                                String url=rec.getString("url");
//                                String video_name=rec.getString("video_name");
//
//                                if (Common.topicRepository.isExist(Integer.parseInt(id))!=1)
//                                {
//                                    classtipTopic topic=new classtipTopic();
//                                    topic.topic_id= id;
//                                    topic.topic_name=topicName;
//                                    topic.chptr_id=chapId;
//                                    topic.priority=priority;
//                                    topic.viewFlag="N";
//                                    topic.updtn_flag="Y";
//                                    topic.topic_type=topicType;
//                                    topic.classmode_id=classmodeId;
//                                    topic.video_id=video_id;
//                                    topic.url=url;
//                                    topic.video_name=video_name;
//
//
//
//                                    Common.topicRepository.insertTopics(topic);
//                                }
//                                else
//                                {
//                                    classtipTopic topic=new classtipTopic();
//                                    topic.topic_id= id;
//                                    topic.topic_name=topicName;
//                                    topic.chptr_id=chapId;
//                                    topic.priority=priority;
//                                    topic.viewFlag="N";
//                                    topic.updtn_flag="Y";
//                                    topic.topic_type=topicType;
//                                    topic.classmode_id=classmodeId;
//                                    topic.video_id=video_id;
//                                    topic.url=url;
//                                    topic.video_name=video_name;
//
//
//                                    Common.topicRepository.updateclasstipTopics(topic);
//                                }
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }));
//    }

//    private void getAllChapterData() {
//        compositeDisposable.add(mService.getChapterByCourse(courseId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnComplete(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                    }
//                })
//                .onExceptionResumeNext(new Observable<List<ChapterTopic>>() {
//                    @Override
//                    protected void subscribeActual(Observer<? super List<ChapterTopic>> observer) {
//                        Log.d("exception123","Chapterexception");
//                    }
//                })
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Toast.makeText(getActivity(), "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .doOnComplete(new Action() {
//                    @Override
//                    public void run() throws Exception {
//
//                        //progressDialog.dismiss();
//                        getAllTopicData();
//                    }
//                })
//                .subscribe(new Consumer<List<ChapterTopic>>() {
//                    @Override
//                    public void accept(List<ChapterTopic> chapterTopics) throws Exception {
//                        try {
//                            JSONArray req = new JSONArray(new Gson().toJson(chapterTopics));
//                            if (req.length()!=Common.chapterRepository.countChapters()){
//                                Common.chapterRepository.emptyChapters();
//                            }
//                            for (int i = 0; i < req.length(); ++i) {
//                                JSONObject rec = req.getJSONObject(i);
//                                String id = String.valueOf(rec.getInt("chapter_id"));
//                                String chapterName = rec.getString("chapter_name");
//                                String subId=rec.getString("sub_id");
//                                String priority=rec.getString("priority");
//
//                                if (Common.chapterRepository.isExist(Integer.parseInt(id))!=1)
//                                {
//                                    classtipChapter chapter=new classtipChapter();
//                                    chapter.chapter_id= id;
//                                    chapter.chapter_name=chapterName;
//                                    chapter.sub_id=subId;
//                                    chapter.priority=priority;
//                                    System.out.println("nuooll = " + null);
//
//                                    Common.chapterRepository.insertChapters(chapter);
//                                }
//                                else
//                                {
//                                    classtipChapter chapter=new classtipChapter();
//                                    chapter.chapter_id= id;
//                                    chapter.chapter_name=chapterName;
//                                    chapter.sub_id=subId;
//                                    chapter.priority=priority;
//
//                                    Common.chapterRepository.updateChapters(chapter);
//                                }
//
//                            }
//                        } catch (JSONException|RuntimeException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }));
//    }

    private void getAllClassmodeData() {
        compositeDisposable.add(mService.getClassmodes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onExceptionResumeNext(new Observable<List<classtipClassMode>>() {
                    @Override
                    protected void subscribeActual(Observer<? super List<classtipClassMode>> observer) {
                        Log.d("exception123", "classmodeexception");
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                       // Toast.makeText(getActivity(), "Net Connection is too slow", Toast.LENGTH_SHORT).show();

                        if (!DownloadUtils.isConnectingToInternet(getActivity())) {
                            DownloadUtils.showCheckConnectiondialog(getActivity());

                        } else {
                            Toast.makeText(getActivity(), "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();

                        }


                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {

                        progressDialog.dismiss();

                       // getAllChapterData();
                    }
                })
                .subscribe(new Consumer<List<classtipClassMode>>() {
                    @Override
                    public void accept(List<classtipClassMode> classModes) throws Exception {
                        classModeAdapterDB.setClassModeList(classModes);
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(classModes));
                            if (req.length()!=Common.classModeRepository.countClassModes()){
                                Common.classModeRepository.emptyClassModes();
                            }
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                String id = String.valueOf(rec.getInt("class_mode_id"));
                                String link = rec.getString("link");
                                String classModeName = rec.getString("class_mode_name");
                                String flag = rec.getString("flag");

                                if (Common.classModeRepository.isExist(Integer.parseInt(id)) != 1) {
                                    classtipClassMode classMode = new classtipClassMode();
                                    classMode.class_mode_id = id;
                                    classMode.class_mode_name = classModeName;
                                    classMode.flag = flag;
                                    classMode.link = link;

                                    Common.classModeRepository.insertClassModes(classMode);
                                } else {
                                    classtipClassMode classMode = new classtipClassMode();
                                    classMode.class_mode_id = id;
                                    classMode.class_mode_name = classModeName;
                                    classMode.flag = flag;
                                    classMode.link = link;

                                    Common.classModeRepository.updateClassmodes(classMode);
                                }

                            }
                        } catch (JSONException | RuntimeException e) {
                            Log.d("exception", "classmode");
                            e.printStackTrace();
                        }
                    }
                }));
    }







//    private void getAllQuestionData(){
//        compositeDisposable.add(mService.getAllQuestions(courseId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .onExceptionResumeNext(new Observable<List<Question>>() {
//                    @Override
//                    protected void subscribeActual(Observer<? super List<Question>> observer) {
//                        Log.d("exception123", "questionexception");
//                    }
//                })
//                .doOnComplete(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                        getAllQuestionYearData();
//                    }
//                })
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Toast.makeText(getActivity(), "Net Connection is too slow", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .subscribe(new Consumer<List<Question>>() {
//                    @Override
//                    public void accept(List<Question> questions) throws Exception {
//
//                        try {
//                            JSONArray req = new JSONArray(new Gson().toJson(questions));
//                            Log.d("question12", new Gson().toJson(questions));
//                            for (int i = 0; i < req.length(); ++i) {
//                                JSONObject rec = req.getJSONObject(i);
//                                int id = rec.getInt("question_id");
//                                int examId = rec.getInt("exam_id");
//                                int topicId = rec.getInt("topic_id");
//                                String question = rec.getString("question_img");
//                                String questionType = rec.getString("question_type");
//                                String questionVideo = rec.getString("question_video");
//                                String orderNo = rec.getString("order_no");
//                                String updationFlag = rec.getString("updtn_flg");
//                                String answerImg=rec.getString("answer_img");
//                                String questionName=rec.getString("question_name");
//                                String vimeoUrl=rec.getString("vimeo_url");
//                                String questionText=rec.getString("question_text");
//                                String examName=rec.getString("exam_name");
////                                String groupNo=rec.getString("group_no");
//                                if (Common.questionRepository.isExist(id) != 1) {
//                                    classtipQuestion classtipQuestion = new classtipQuestion();
//                                    classtipQuestion.question_id = String.valueOf(id);
//                                    classtipQuestion.question_video = questionVideo;
//                                    classtipQuestion.exam_id = String.valueOf(examId);
//                                    classtipQuestion.topic_id = String.valueOf(topicId);
//                                    classtipQuestion.question_img = question;
//                                    classtipQuestion.question_type = questionType;
//                                    classtipQuestion.order_no = orderNo;
//                                    classtipQuestion.answer_img=answerImg;
//                                    classtipQuestion.question_name=questionName;
//                                    classtipQuestion.vimeo_url=vimeoUrl;
//                                    classtipQuestion.question_text=questionText;
//                                    classtipQuestion.exam_name = examName;
////                                    classtipQuestion.groupNo=groupNo;
//
//                                    Common.questionRepository.insertQuestions(classtipQuestion);
//                                } else {
//                                    classtipQuestion classtipQuestion = new classtipQuestion();
//                                    classtipQuestion.question_id = String.valueOf(id);
//                                    classtipQuestion.question_video = questionVideo;
//                                    classtipQuestion.exam_id = String.valueOf(examId);
//                                    classtipQuestion.topic_id = String.valueOf(topicId);
//                                    classtipQuestion.question_img = question;
//                                    classtipQuestion.question_type = questionType;
//                                    classtipQuestion.order_no = orderNo;
//                                    classtipQuestion.exam_name = examName;
//
//                                    Common.questionRepository.updateQuestions(classtipQuestion);
//                                }
//                            }
//                        } catch (JSONException | RuntimeException e) {
//                            Log.d("exception123", "question");
//                            e.printStackTrace();
//                        }
//                    }
//                }));
//    }

//    private void getAllQuestionYearData(){
//        compositeDisposable.add(mService.getAllYearWiseQuestion(courseId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnComplete(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                        getAllExamData();
//                    }
//                })
//                .subscribe(new Consumer<List<QuestionYear>>() {
//                    @Override
//                    public void accept(List<QuestionYear> questionYears) throws Exception {
//
//                        try {
//                            JSONArray req = new JSONArray(new Gson().toJson(questionYears));
//                            Log.d("classtip23", new Gson().toJson(questionYears));
//                            for (int i = 0; i < req.length(); ++i) {
//                                JSONObject rec = req.getJSONObject(i);
//                                int id= rec.getInt("question_id");
//                                int examId= rec.getInt("exam_id");
//                                int groupNo= rec.getInt("group_no");
//                                int topicId= rec.getInt("topic_id");
//                                String question = rec.getString("question_img");
//                                String questionType = rec.getString("question_type");
//                                String questionVideo=rec.getString("question_video");
//                                String orderNo=rec.getString("order_no");
//                                String answerImage=rec.getString("answer_img");
//                                if (Common.questionYearRepository.isExist(id)!=1)
//                                {
//                                    classtipQuestionYear classtipQuestion =new classtipQuestionYear();
//                                    classtipQuestion.id= String.valueOf(id);
//                                    classtipQuestion.questionVideo=questionVideo;
//                                    classtipQuestion.examId= String.valueOf(examId);
//                                    classtipQuestion.topicId= String.valueOf(topicId);
//                                    classtipQuestion.question=question;
//                                    classtipQuestion.questionType=questionType;
//                                    classtipQuestion.groupNo= String.valueOf(groupNo);
//                                    classtipQuestion.orderNo=orderNo;
//                                    classtipQuestion.answerImage=answerImage;
//
//                                    Common.questionYearRepository.insertQuestions(classtipQuestion);
//                                }
//                                else
//                                {
//                                }
//
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }));
//    }

//    private void getAllVideoData(){
//        compositeDisposable.add(mService.getMainVideos(courseId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .onExceptionResumeNext(new Observable<List<MainVideo>>() {
//                    @Override
//                    protected void subscribeActual(Observer<? super List<MainVideo>> observer) {
//                        Log.d("exception123", "videoexception");
//                    }
//                })
//                .doOnComplete(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                        getAllQuestionData();
//                    }
//                })
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Toast.makeText(getActivity(), "Net Connection is too slow", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .subscribe(new Consumer<List<MainVideo>>() {
//                    @Override
//                    public void accept(List<MainVideo> mainVideos) throws Exception {
//                        try {
//                            JSONArray req = new JSONArray(new Gson().toJson(mainVideos));
//                            Log.d("main", new Gson().toJson(mainVideos));
//                            for (int i = 0; i < req.length(); ++i) {
//                                JSONObject rec = req.getJSONObject(i);
//                                int id = rec.getInt("video_id");
//                                String videoName = rec.getString("video_name");
//                                String videoType = rec.getString("video_type");
//                                int topicId = rec.getInt("topic_id");
//                                String chapterId = rec.getString("chapter_id");
//                                String url = rec.getString("url");
//                                String updationFlag = rec.getString("updtn_flg");
//
//                                if (Common.recentRepository.isExist(id) != 1) {
//                                    classtipRecent classtipRecentItem = new classtipRecent();
//                                    classtipRecentItem.id = id;
//                                    classtipRecentItem.videoName = videoName;
//                                    classtipRecentItem.videoType = videoType;
//                                    classtipRecentItem.url = url;
//                                    classtipRecentItem.topicId = String.valueOf(topicId);
//                                    classtipRecentItem.chapterId = chapterId;
//
//                                    Common.recentRepository.insertVideos(classtipRecentItem);
//                                } else {
//                                    classtipRecent classtipRecentItem = new classtipRecent();
//                                    classtipRecentItem.id = id;
//                                    classtipRecentItem.videoName = videoName;
//                                    classtipRecentItem.videoType = videoType;
//                                    classtipRecentItem.url = url;
//                                    classtipRecentItem.topicId = String.valueOf(topicId);
//                                    classtipRecentItem.chapterId = String.valueOf(chapterId);
//
//                                    Common.recentRepository.updateVideos(classtipRecentItem);
//                                }
//                            }
//                        } catch (JSONException | RuntimeException e) {
//                            Log.d("exception123", "mainvideo");
//                            e.printStackTrace();
//                        }
//                    }
//                }));
//    }

//    private void getAllExamData(){
//        compositeDisposable.add(mService.getExamsByCourseId(courseId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnComplete(new Action() {
//                    @Override
//                    public void run() throws Exception {
//                        appPreferences.saveBoolean(BULK_DATA_STATUS, true);
//                        progressDialog.dismiss();
//                    }
//                })
//                .onExceptionResumeNext(new Observable<List<Exams>>() {
//                    @Override
//                    protected void subscribeActual(Observer<? super List<Exams>> observer) {
//
//                    }
//                })
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//
//                    }
//                })
//                .subscribe(new Consumer<List<Exams>>() {
//                    @Override
//                    public void accept(List<Exams> exams) throws Exception {
//                        JSONArray req=new JSONArray(new Gson().toJson(exams));
//                        for (int i=0; i<req.length();++i)
//                        {
//                            JSONObject rec=req.getJSONObject(i);
//                            String id = String.valueOf(rec.getInt("exam_id"));
//                            String courseId= String.valueOf(rec.getInt("course_id"));
//                            String subId= String.valueOf(rec.getInt("sub_id"));
//                            String examName = rec.getString("exam_name");
//
//                            if (Common.examRepository.isExist(Integer.parseInt(id))!=1)
//                            {
//                                classtipExam exam=new classtipExam();
//                                exam.id= id;
//                                exam.examName=examName;
//                                exam.courseId=courseId;
//                                exam.subId=subId;
//
//                                Common.examRepository.insertExams(exam);
//                            }
//                            else
//                            {
//
//                            }
//                        }
//                    }
//                }));
//    }

    private void getClassModeDB() {
        compositeDisposable.add(Common.classModeRepository.getClassModes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipClassMode>>() {
                    @Override
                    public void accept(List<classtipClassMode> classtipClassModes) throws Exception {
                        classModeAdapterDB.setClassModeList(classtipClassModes);
                    }
                }));
    }

    private void getSubjectDB(String courseId) {
        System.out.println("courseId = " + courseId);
        compositeDisposable.add(Common.subjectRepository.getSubjectsById(Integer.parseInt(courseId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipSubject>>() {
                    @Override
                    public void accept(List<classtipSubject> classtipSubjects) throws Exception {
                        subjectAdapterDB.setSubjectList(classtipSubjects);
                    }
                }));
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

}
