package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipRecentDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/5/2018.
 */

public class classtipRecentDataSource implements IclasstipRecentDataSource {

    private classtipRecentDAO recentDAO;
    private static classtipRecentDataSource instance;

    public classtipRecentDataSource(classtipRecentDAO recentDAO) {
        this.recentDAO = recentDAO;
    }

    public static classtipRecentDataSource getInstance(classtipRecentDAO recentDAO){
        if (instance==null)
            instance=new classtipRecentDataSource(recentDAO);
                    return instance;
    }

    @Override
    public Flowable<List<classtipRecent>> getRecentVideos() {
        return recentDAO.getRecentVideos();
    }

    @Override
    public List<classtipRecent> getRecentVideosById(int topicId) {
        return recentDAO.getRecentVideosById(topicId);
    }

    @Override
    public int isExist(int videoId) {
        return recentDAO.isExist(videoId);
    }

    @Override
    public int countVideos() {
        return recentDAO.countVideos();
    }

    @Override
    public void emptyVideos() {
        recentDAO.emptyVideos();
    }

    @Override
    public void insertVideos(classtipRecent... classtipRecents) {
        recentDAO.insertVideos(classtipRecents);
    }

    @Override
    public void updateVideos(classtipRecent... classtipRecents) {
        recentDAO.updateVideos(classtipRecents);
    }

    @Override
    public void deleteVideos(classtipRecent... classtipRecents) {
        recentDAO.deleteVideos(classtipRecents);
    }
}
