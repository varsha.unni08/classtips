package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/7/2018.
 */
@Entity(tableName = "classtipSubject")
public class classtipSubject {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name="subject_id")
    public String subject_id;

    @ColumnInfo(name="subject_name")
    public String subject_name;

    @ColumnInfo(name="course_id")
    public String course_id;

    @ColumnInfo(name="link")
    public String link;

    @ColumnInfo(name="prize")
    public String prize;

    @ColumnInfo(name="ispurchased")
    public boolean ispurchased;
}
