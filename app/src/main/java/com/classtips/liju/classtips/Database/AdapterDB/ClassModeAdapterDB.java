package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.classtips.liju.classtips.Adapter.ClassmodeViewHolder;
import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.SubjectActivity;
import com.classtips.liju.classtips.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 9/24/2018.
 */

public class ClassModeAdapterDB extends RecyclerView.Adapter<ClassmodeViewHolder> {
    Context context;
    List<classtipClassMode> classtipClassModes;

    public ClassModeAdapterDB(Context context, List<classtipClassMode> classtipClassModes) {
        this.context = context;
        this.classtipClassModes = classtipClassModes;
    }

    @Override
    public ClassmodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.classmode_layout,null);
        return new ClassmodeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClassmodeViewHolder holder, final int position) {
        Picasso.with(context)
                .load(classtipClassModes.get(position).link)
                .into(holder.image_classmod);
        holder.txt_clsmod_name.setText(classtipClassModes.get(position).class_mode_name);
        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                Common.currentClasstipClassMode=classtipClassModes.get(position);
                Common.currentClassmode=null;
                context.startActivity(new Intent(context,SubjectActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return classtipClassModes.size();
    }

    public void setClassModeList(List<classtipClassMode> classtipClassModes) {
        this.classtipClassModes = classtipClassModes;
        notifyDataSetChanged();
    }
}
