package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/8/2018.
 */

public interface IclasstipTopicDataSource {

    Flowable<List<classtipTopic>> getTopics();

    Flowable<List<classtipTopic>> getTopicsById(int chapId,String classmodeId);

    Flowable<List<classtipTopic>> getFavoritessById();

    int isExist(int topicId);

    int isViewed(String flag,int topicId);

    int isUpdated(String flag,int topicId);

    int isFavorite(int topicId);

    int countTopics();

    void insertTopics(classtipTopic...classtipTopics);

    void updateTopicsById(String subId,String subName,String chapName,String flag, int topicId);

    void updateTopics(String flag, int topicId);

    void updateFavorite(int topicId);

    void removeFavorite(int topicId);

    void updateclasstipTopics(classtipTopic...classtipTopics);
}
