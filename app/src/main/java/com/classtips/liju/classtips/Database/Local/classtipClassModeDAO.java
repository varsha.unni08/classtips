package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/24/2018.
 */
@Dao
public interface classtipClassModeDAO {


    @Query("SELECT * FROM classtipClassMode")
    Flowable<List<classtipClassMode>> getClassModes();

    @Query("SELECT EXISTS(SELECT 1 FROM classtipClassMode where class_mode_id=:classModeId)")
    int isExist(int classModeId);

    @Query("SELECT COUNT(*) FROM classtipClassMode")
    int countClassModes();

    @Query("Delete  from classtipClassMode")
    void  emptyClassModes();

    @Insert
    void insertClassModes(classtipClassMode...classtipClassModes);

    @Update
    void updateClassmodes(classtipClassMode...classtipClassModes);

}
