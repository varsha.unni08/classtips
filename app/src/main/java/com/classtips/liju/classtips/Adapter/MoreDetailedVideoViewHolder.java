package com.classtips.liju.classtips.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

public class MoreDetailedVideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView image_video;
    public TextView txt_description;
    public CardView cardView;

    IItemClickListner itemClickListner;

    public void setItemClickListner(IItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }


    public MoreDetailedVideoViewHolder(@NonNull View itemView) {
        super(itemView);

        image_video=(ImageView)itemView.findViewById(R.id.image_video);
        txt_description=(TextView)itemView.findViewById(R.id.txt_description);
        cardView=(CardView)itemView.findViewById(R.id.card_view) ;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListner.onClick(v);
    }
}
