package com.classtips.liju.classtips;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.classtips.liju.classtips.Adapter.ClassmodeAdapter;
import com.classtips.liju.classtips.Adapter.SubjectAdapter;
import com.classtips.liju.classtips.Database.AdapterDB.ClassModeAdapterDB;
import com.classtips.liju.classtips.Database.AdapterDB.SubjectAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;
import com.classtips.liju.classtips.Database.ModelDB.classtipExam;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;
import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Fragment.DownloadFragment;
import com.classtips.liju.classtips.Fragment.FavoritesFragment;
import com.classtips.liju.classtips.Fragment.HomeFragment;
import com.classtips.liju.classtips.Model.ChapterTopic;
import com.classtips.liju.classtips.Model.ClassMode;
import com.classtips.liju.classtips.Model.Exams;
import com.classtips.liju.classtips.Model.MainVideo;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.Model.QuestionYear;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.Model.Topic;
import com.classtips.liju.classtips.Model.UpdateFlag;
import com.classtips.liju.classtips.Model.User;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Update.UpdateHelper;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.config.AppPreferences;
import com.google.gson.Gson;
import com.nex3z.notificationbadge.NotificationBadge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.classtips.liju.classtips.config.ApplicationConstants.APPLICATION_DATA;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UpdateHelper.OnUpdateCheckListener {
    TextView txt_name, txt_phone;
    NotificationBadge badge;
    ImageView cart_icon;
    RelativeLayout rootLayout;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    final int RequestImeiPermissionID = 1001;
    String phone, name, deviceID;
    AppPreferences appPreferences;
    IClassTipAPI mService;
    //RxJava
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(HomeActivity.this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        rootLayout= findViewById(R.id.rootLayout);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        mService = Common.getAPI();
        deviceID = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        txt_name = headerView.findViewById(R.id.txt_name);
        txt_phone = headerView.findViewById(R.id.txt_phone);
        appPreferences = AppPreferences.getInstance(HomeActivity.this, APPLICATION_DATA);

        loadFragment(new HomeFragment());

        compositeDisposable.add(Common.userRepository.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipUser>>() {
                    @Override
                    public void accept(List<classtipUser> classtipUsers) throws Exception {
                        Log.d("classtip234", new Gson().toJson(classtipUsers));
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                 phone = rec.getString("phone");
                                 name = rec.getString("name");
                            }
                            txt_name.setText(name);
                            txt_phone.setText(phone);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (isConnectingToInternet()) {
                            UpdateHelper.with(HomeActivity.this)
                                    .onUpdateCheck(HomeActivity.this)
                                    .check();
                            mService.getUserInformation(phone)
                                    .enqueue(new Callback<User>() {
                                        @Override
                                        public void onResponse(Call<User> call, Response<User> response) {

                                            if(response.body()!=null) {
                                                if (!response.body().getImei().equals(deviceID)) {
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                                                    builder.setMessage("You are already logged in another device");
                                                    builder.setCancelable(false)
                                                            .setPositiveButton("Verify", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    appPreferences.clearData();
                                                                    Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                    startActivity(intent);
                                                                }
                                                            });
                                                    builder.show();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<User> call, Throwable t) {

                                        }
                                    });
                        }
                    }
                }));

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            Fragment fragment=null;
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId()==R.id.action_home)
                    fragment=new HomeFragment();
                if (menuItem.getItemId()==R.id.action_favorites)
                    fragment=new FavoritesFragment();
                if (menuItem.getItemId()==R.id.action_download)
                    fragment=new DownloadFragment();

                return loadFragment(fragment);
            }
        });

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment!=null)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,fragment)
                    .commit();
            return true;
        }
        return false;
    }
    //Exit application when clicked back button
    Boolean isBackButtonClicked = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (isBackButtonClicked) {
                finish();
            } else
                {
                    this.isBackButtonClicked = true;
                    Toast.makeText(this, "Please click BACK again to exit from Classtip", Toast.LENGTH_SHORT).show();
                }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action_bar, menu);
        View view = menu.findItem(R.id.cart_menu).getActionView();
        badge = view.findViewById(R.id.badge);
        cart_icon = view.findViewById(R.id.cart_icon);
        cart_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, CartActivity.class));
            }
        });
        updateCount();
        return true;
    }

    private void updateCount() {
        if (badge == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Common.purchaseRepository.countPurchases() == 0)
                    badge.setVisibility(View.INVISIBLE);
                else
                    badge.setVisibility(View.VISIBLE);
                badge.setText(String.valueOf(Common.purchaseRepository.countPurchases()));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.cart_menu) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id==R.id.action_home)
        {
           Fragment fragment=new HomeFragment();
           loadFragment(fragment);
        }


        if (id == R.id.nav_wallet) {
            startActivity(new Intent(HomeActivity.this, WalletActivity.class));
        } else if (id == R.id.nav_purchase) {
            startActivity(new Intent(HomeActivity.this, PurchaseActivity.class));

        } else if (id == R.id.nav_qrcode) {
            startActivity(new Intent(HomeActivity.this, QRcodeActivity.class));

        }  else if (id == R.id.nav_share) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("About Us");
            alertDialogBuilder.setIcon(R.mipmap.classtiplogo);
            alertDialogBuilder.setMessage("Email:classtiptution@gmail.com\nPhone:8089379353");
            alertDialogBuilder.setNeutralButton("Call us", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:8089379353"));

                    if (ActivityCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        ActivityCompat.requestPermissions(HomeActivity.this,
                                new String[]{android.Manifest.permission.CALL_PHONE},RequestImeiPermissionID);
                        return;
                    }
                    startActivity(callIntent);
                }
            }).show();
        } else if (id == R.id.nav_send) {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Classtips");
                String shareMessage= "\nLet me recommend you this educational application\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch(Exception e) {
                //e.toString();
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            return;
        }
    }

    @Override
    protected void onResume() {
        isBackButtonClicked=false;
        super.onResume();
        updateCount();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onUpdateCheckListener(final String urlApp) {
        AlertDialog alertDialog=new AlertDialog.Builder(this)
                .setTitle("New Version Available")
                .setMessage("Please update to new version to continue use")
                .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent updateIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(urlApp));
                        startActivity(updateIntent);
                    }
                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
        alertDialog.show();
    }
}