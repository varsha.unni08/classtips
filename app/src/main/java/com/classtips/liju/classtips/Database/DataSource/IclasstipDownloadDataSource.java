package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipDownload;

/**
 * Created by User on 10/26/2018.
 */

public interface IclasstipDownloadDataSource {

    int isExist(int downloadId);

    int isFlgExist(String flag, int downloadId);

    int countSubjects();

    void updateDownloads(String flag, int downloadId);

    void  emptyDownloads();

    void insertDownloads(classtipDownload...classtipDownloads);

    void updateDownloads(classtipDownload...classtipDownloads);

    void deleteDownloads(classtipDownload...classtipDownloads);
}
