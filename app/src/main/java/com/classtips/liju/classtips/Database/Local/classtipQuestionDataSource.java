package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipQuestionDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/27/2018.
 */

public class classtipQuestionDataSource implements IclasstipQuestionDataSource {

    private classtipQuestionDAO questionDAO;
    private static classtipQuestionDataSource instance;

    public classtipQuestionDataSource(classtipQuestionDAO questionDAO) {
        this.questionDAO = questionDAO;
    }

    public static classtipQuestionDataSource getInstance(classtipQuestionDAO questionDAO){
        if (instance==null)
            instance=new classtipQuestionDataSource(questionDAO);
        return instance;
    }

    @Override
    public Flowable<List<classtipQuestion>> getQuestions() {
        return questionDAO.getQuestions();
    }

    @Override
    public Flowable<List<classtipQuestion>> getQuestionsById(int topicId) {
        return questionDAO.getQuestionsById(topicId);
    }

    @Override
    public Flowable<List<classtipQuestion>> getQuestionsById(int topicId, int orderNo) {
        return questionDAO.getQuestionsById(topicId, orderNo);
    }

    @Override
    public int isExist(int questionId) {
        return questionDAO.isExist(questionId);
    }

    @Override
    public int countQuestions(int topicId) {
        return questionDAO.countQuestions(topicId);
    }

    @Override
    public int countQuestions() {
        return questionDAO.countQuestions();
    }

    @Override
    public void insertQuestions(classtipQuestion... classtipQuestions) {
        questionDAO.insertQuestions(classtipQuestions);
    }

    @Override
    public void updateQuestions(classtipQuestion... classtipQuestions) {
        questionDAO.updateQuestions(classtipQuestions);
    }
}
