package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/27/2018.
 */
@Entity(tableName = "classtipQuestion")
public class classtipQuestion {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="question_id")
    public String question_id;

    @ColumnInfo(name="exam_id")
    public String exam_id;

    @ColumnInfo(name="question_img")
    public String question_img;

    @ColumnInfo(name="exam_name")
    public String exam_name;

    @ColumnInfo(name="question_video")
    public String question_video;

    @ColumnInfo(name="topic_id")
    public String topic_id;

    @ColumnInfo(name="question_type")
    public String question_type;

    @ColumnInfo(name="order_no")
    public String order_no;

    @ColumnInfo(name="question_text")
    public String question_text;

    @ColumnInfo(name="vimeo_url")
    public String vimeo_url;

    @ColumnInfo(name="question_name")
    public String question_name;

    @ColumnInfo(name="answer_img")
    public String answer_img;

    @ColumnInfo(name="group_no")
    public String group_no;

    @ColumnInfo(name="question_flag")
    public String question_flag;

    @ColumnInfo(name="course_id")
    public String course_id;

    @ColumnInfo(name="updtn_flg")
    public String updtn_flg;


}
