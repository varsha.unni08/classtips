package com.classtips.liju.classtips.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;

/**
 * Created by User on 8/22/2018.
 */

public class QuestionFragment extends Fragment {
    //VideoView videoView;
    private VideoView videoView;
    IClassTipAPI mService;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mService= Common.getAPI();
        View root = inflater.inflate(R.layout.fragment_layout, container, false);

        videoView = (VideoView) root.findViewById(R.id.video_view);

//        videoView.setVideoPath(
//                "http://www.ebookfrenzy.com/android_book/movie.mp4");
        videoView.setVideoPath(Common.currentQuestion.question_video);

        videoView.start();

        return root;
        //return inflater.inflate(R.layout.fragment_layout, container, false);
    }
}
