package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/27/2018.
 */

public interface IclasstipQuestionDataSource {

    Flowable<List<classtipQuestion>> getQuestions();

    Flowable<List<classtipQuestion>> getQuestionsById(int topicId);

    Flowable<List<classtipQuestion>> getQuestionsById(int topicId,int orderNo);

    int isExist(int questionId);

    int countQuestions(int topicId);

    int countQuestions();

    void insertQuestions(classtipQuestion...classtipQuestions);

    void updateQuestions(classtipQuestion...classtipQuestions);
}
