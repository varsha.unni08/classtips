package com.classtips.liju.classtips;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.AdapterDB.ChapterAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.ChapterTopic;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.DownloadUtils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChapterActivity extends AppCompatActivity {
    IClassTipAPI mService;
    RecyclerView chapter_menu;
    ProgressBar progressBar = null;
    String phone,subId;
    ChapterAdapterDB chapterAdapterDB;
    //RxJava
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);

        mService = Common.getAPI();
        progressBar = findViewById(R.id.progressBar);
        chapter_menu = findViewById(R.id.chapter_menu);
        chapter_menu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        chapter_menu.setHasFixedSize(true);

        chapterAdapterDB = new ChapterAdapterDB(this, new ArrayList<classtipChapter>());
        chapter_menu.setAdapter(chapterAdapterDB);


        if(Common.currentClasstipSubject!=null) {
            subId = Common.currentClasstipSubject.subject_id;

            compositeDisposable.add(Common.userRepository.getUsers()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<classtipUser>>() {
                        @Override
                        public void accept(List<classtipUser> classtipUsers) throws Exception {
                            Log.d("classtip234", new Gson().toJson(classtipUsers));

                            if(classtipUsers.size()>0) {

                                try {
                                    JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                                    for (int i = 0; i < req.length(); ++i) {
                                        JSONObject rec = req.getJSONObject(i);
                                        phone = rec.getString("phone");
                                    }
                                    if (isConnectingToInternet()) {
                                        progressBar.setVisibility(View.VISIBLE);
                                        loadChapterList(subId);
                                       // checkSujectPurchase(phone, subId);
                                    }
                                    loadChapterDB(subId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            else {


                            }
                        }
                    }));
        }
    }




    private void checkSujectPurchase(String phone, String subId) {
        mService.checkPurchaseExists(phone, subId)
                .enqueue(new Callback<CheckPurchaseResponse>() {
                    @Override
                    public void onResponse(Call<CheckPurchaseResponse> call, Response<CheckPurchaseResponse> response) {
                        CheckPurchaseResponse purchaseResponse = response.body();
                        if (purchaseResponse.isExists()) {
                        } else {
//                            AlertDialog.Builder builder = new AlertDialog.Builder(ChapterActivity.this);
//                            LayoutInflater factory = LayoutInflater.from(ChapterActivity.this);
//                            final View view = factory.inflate(R.layout.alert_dialog_image, null);
//                            builder.setView(view);
//                            builder.setMessage("Do You Want To PURCHASE  ?");
//                            builder.setCancelable(true)
//                                    .setPositiveButton("Purchase Now", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            Intent intent = new Intent(ChapterActivity.this, PurchaseActivity.class);
//                                            startActivity(intent);
//                                        }
//                                    }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.cancel();
//
//                                }
//                            });
//                            builder.show();
                          //  dialogToTokenExpired("Purchase", "Do you want to purchase this subject", "Later", "Ok");
                        }
                    }

                    @Override
                    public void onFailure(Call<CheckPurchaseResponse> call, Throwable t) {

                    }
                });
    }

    private void loadChapterDB(String subject_id) {
        compositeDisposable.add(Common.chapterRepository.getChaptersById(Integer.parseInt(subject_id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipChapter>>() {
                    @Override
                    public void accept(List<classtipChapter> classtipChapters) throws Exception {

                        if(classtipChapters.size()==0) {

                            if (!DownloadUtils.isConnectingToInternet(ChapterActivity.this)) {
                                DownloadUtils.showCheckConnectiondialog(ChapterActivity.this);

                            } else {
                                //Toast.makeText(ChapterActivity.this, "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();

                            }
                        }
                        else {
                            chapterAdapterDB.setChaptertList(classtipChapters);
                        }
                    }
                }));
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private void loadChapterList(final String subId) {
        compositeDisposable.add(mService.getChapter(subId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onExceptionResumeNext(new Observable<List<classtipChapter>>() {
                    @Override
                    protected void subscribeActual(Observer<? super List<classtipChapter>> observer) {
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        //Toast.makeText(ChapterActivity.this, "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();

                        if (!DownloadUtils.isConnectingToInternet(ChapterActivity.this)) {
                            DownloadUtils.showCheckConnectiondialog(ChapterActivity.this);

                        } else {
                            Toast.makeText(ChapterActivity.this, "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();

                        }


                    }
                })
                .doOnEach(new Consumer<Notification<List<classtipChapter>>>() {
                    @Override
                    public void accept(Notification<List<classtipChapter>> listNotification) throws Exception {
                        progressBar.setVisibility(View.GONE);
                    }
                })
                .subscribe(new Consumer<List<classtipChapter>>() {
                    @Override
                    public void accept(List<classtipChapter> chapterTopics) throws Exception {
                        chapterAdapterDB.setChaptertList(chapterTopics);
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(chapterTopics));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                String id = String.valueOf(rec.getInt("chapter_id"));
                                String chapterName = rec.getString("chapter_name");
                                String subId = rec.getString("sub_id");
                                String priority = rec.getString("priority");

                                if (Common.chapterRepository.isExist(Integer.parseInt(id)) != 1) {
                                    classtipChapter chapter = new classtipChapter();
                                    chapter.chapter_id = id;
                                    chapter.chapter_name = chapterName;
                                    chapter.sub_id = subId;
                                    chapter.priority = priority;

                                    Common.chapterRepository.insertChapters(chapter);
                                } else {

                                }
                            }
                        } catch (JSONException | RuntimeException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    @SuppressLint("NewApi")
    public void dialogToTokenExpired(String title, String message, String negativeText, String positiveText) {
        final Dialog dialog = new Dialog(ChapterActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_otp_send_alert);

        TextView titleTextView = dialog.findViewById(R.id.titleTextView);
        TextView contentTextView = dialog.findViewById(R.id.contentTextView);
        TextView cancelTextView = dialog.findViewById(R.id.sendToPhoneTextView);
        TextView enrollFingerTextView = dialog.findViewById(R.id.sendToEmailTextView);

        titleTextView.setText(title);
        contentTextView.setText(message);
        cancelTextView.setText(negativeText);
        enrollFingerTextView.setText(positiveText);

        dialog.findViewById(R.id.closeCardView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        enrollFingerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChapterActivity.this, PurchaseActivity.class);
                startActivity(intent);
                dialog.cancel();
                finish();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
