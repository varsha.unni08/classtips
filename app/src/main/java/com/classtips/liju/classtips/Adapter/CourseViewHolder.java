package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by User on 8/17/2018.
 */

public class CourseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    //RadioGroup radioGroup;
    RadioButton radio_course;
    //TextView course_type;

    IItemClickListner itemClickListner;

    public void setItemClickListner(IItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public CourseViewHolder(View itemView) {
        super(itemView);
       // radioGroup=(RadioGroup)itemView.findViewById(R.id.radiogrp_course);
        radio_course=(RadioButton)itemView.findViewById(R.id.radio_course);
        //course_type=(TextView)itemView.findViewById(R.id.course_name);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListner.onClick(v);
    }
}
