package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/15/2018.
 */

public class classtipPurchaseRepository implements IclasstipPurchaseDataSource {

    private IclasstipPurchaseDataSource iclasstipPurchaseDataSource;
    private static classtipPurchaseRepository instance;

    public classtipPurchaseRepository(IclasstipPurchaseDataSource iclasstipPurchaseDataSource) {
        this.iclasstipPurchaseDataSource = iclasstipPurchaseDataSource;
    }

    public static classtipPurchaseRepository getInstance(IclasstipPurchaseDataSource iclasstipPurchaseDataSource){
        if (instance==null)
            instance=new classtipPurchaseRepository(iclasstipPurchaseDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipPurchase>> getPurchaseList() {
        return iclasstipPurchaseDataSource.getPurchaseList();
    }

    @Override
    public int isExist(int subId) {
        return iclasstipPurchaseDataSource.isExist(subId);
    }

    @Override
    public int sumPrice() {
        return iclasstipPurchaseDataSource.sumPrice();
    }

    @Override
    public int countPurchases() {
        return iclasstipPurchaseDataSource.countPurchases();
    }

    @Override
    public void emptyPurchases() {
        iclasstipPurchaseDataSource.emptyPurchases();
    }

    @Override
    public void insertPurchases(classtipPurchase... classtipPurchases) {
        iclasstipPurchaseDataSource.insertPurchases(classtipPurchases);
    }

    @Override
    public void deletePurchases(classtipPurchase... classtipPurchases) {
        iclasstipPurchaseDataSource.deletePurchases(classtipPurchases);
    }
}
