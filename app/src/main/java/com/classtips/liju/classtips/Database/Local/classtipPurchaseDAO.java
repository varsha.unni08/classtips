package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/15/2018.
 */
@Dao
public interface classtipPurchaseDAO {

    @Query("SELECT * FROM classtipPurchase")
    Flowable<List<classtipPurchase>> getPurchaseList();


    @Query("SELECT EXISTS(SELECT 1 FROM classtipPurchase where subId=:subId)")
    int isExist(int subId);

    @Query("SELECT SUM(price) FROM classtipPurchase")
    int sumPrice();

    @Query("SELECT COUNT(*) FROM classtipPurchase")
    int countPurchases();

    @Query("Delete  from classtipPurchase")
    void  emptyPurchases();

    @Insert
    void insertPurchases(classtipPurchase...classtipPurchases);

    @Delete
    void deletePurchases(classtipPurchase...classtipPurchases);
}
