package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.Topic;
import com.classtips.liju.classtips.QuestionActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.VideoHomeActivity;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by User on 8/16/2018.
 */

public class TopicAdapter extends RecyclerView.Adapter<TopicViewHolder> {
    Context context;
    List<Topic> topics;
    List<Integer> questionCount;
    //List<ChapterTopic> chapterTopics;

    public TopicAdapter(Context context, List<Topic> topics, List<Integer> questionCount) {
        this.context = context;
        this.topics = topics;
        this.questionCount=questionCount;
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.topic_item_layout,null);
        return new TopicViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TopicViewHolder holder, final int position) {
        holder.text_topic_name.setText(topics.get(position).topic_name);
        holder.questiom_count.setText("No of questions "+questionCount.get(position).toString());

        if (Common.topicRepository.isViewed("N",Integer.parseInt(topics.get(position).topic_id))==1)
            holder.cardView.setCardBackgroundColor(Color.WHITE);


        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, Common.currentClassmode.flag, Toast.LENGTH_SHORT).show();
                Common.currentTopic=topics.get(position);
                Common.currentClasstipTopic=null;


                try {
                    try {
                        if ("q1".equals(Common.currentClassmode.flag)) {
                            Intent intent=new Intent(context,QuestionActivity.class);
                            intent.putExtra("key","1");
                            context.startActivity(intent);
                        } else
                            context.startActivity(new Intent(context, VideoHomeActivity.class));
                    } catch (NullPointerException e) {
                        if ("q1".equals(Common.currentClasstipClassMode.flag)) {
                            Intent intent=new Intent(context,QuestionActivity.class);
                            intent.putExtra("key","1");
                            context.startActivity(intent);
                        } else
                            context.startActivity(new Intent(context, VideoHomeActivity.class));
                    }
                }catch (NullPointerException e)
                {
                    context.startActivity(new Intent(context, VideoHomeActivity.class));
//                ((Activity) context).finish();
                }

                //Checking Classmode is Video or Question


                if (Common.topicRepository.isViewed("N",Integer.parseInt(topics.get(position).topic_id))==1){

                    int id= Integer.parseInt(topics.get(position).topic_id);
                    String viewFlag="Y";
                    Common.topicRepository.updateTopics(viewFlag,id);

                }

                if (Common.topicRepository.isExist(Integer.parseInt(topics.get(position).topic_id))!=1)
                {
//                Toast.makeText(context, "T added", Toast.LENGTH_SHORT).show();
                    classtipTopic topic=new classtipTopic();
                    topic.topic_id=topics.get(position).topic_id;
                    topic.topic_name=topics.get(position).topic_name;
                    topic.subId=Common.currentSubject.subject_id;
                    topic.subName=Common.currentSubject.subject_name;
                    topic.chptr_id=topics.get(position).chptr_id;
                    topic.chapName=Common.currentChapter.chapter_name;
                    topic.priority=topics.get(position).priority;
                    topic.topic_type=topics.get(position).topic_type;
                    topic.viewFlag=topics.get(position).updtn_flag;


                    Common.topicRepository.insertTopics(topic);
                    Log.d("classtip23", new Gson().toJson(topic));
//                Toast.makeText(context, new Gson().toJson(Common.topicRepository.countTopics()), Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Favorite system
        String classmodeType;
        try{
            classmodeType=Common.currentClassmode.class_mode_id;
        }catch (Exception e){
            classmodeType="v1";
        }

        if ("v1".equals(classmodeType))
        {
            if (Common.topicRepository.isFavorite(Integer.parseInt(topics.get(position).topic_id))==1)
                holder.image_favorite.setImageResource(R.drawable.ic_favorite_white_24dp);
            else
                holder.image_favorite.setImageResource(R.drawable.ic_favorite_border_white_24dp);

            holder.image_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Common.topicRepository.isFavorite(Integer.parseInt(topics.get(position).topic_id))!=1)
                    {
                        Common.topicRepository.updateFavorite(Integer.parseInt(topics.get(position).topic_id));
                        holder.image_favorite.setImageResource(R.drawable.ic_favorite_white_24dp);


                    }else
                    {
                        Common.topicRepository.removeFavorite(Integer.parseInt(topics.get(position).topic_id));
                        holder.image_favorite.setImageResource(R.drawable.ic_favorite_border_white_24dp);
                    }
                }
            });
        }else
        {
            holder.image_favorite.setVisibility(View.GONE);
        }

    }


    @Override
    public int getItemCount() {
        return topics.size();
    }

}
