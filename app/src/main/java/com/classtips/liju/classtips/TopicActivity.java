package com.classtips.liju.classtips;

import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.AdapterDB.TopicAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.DownloadUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.classtips.liju.classtips.Utils.Common.currentClasstipQuestion;
import static com.classtips.liju.classtips.Utils.Common.questionRepository;

public class TopicActivity extends AppCompatActivity {
    IClassTipAPI mService;
    RecyclerView topic_menu;
    String classmodeType, classmodeId, subjectId, subjectName, chapterName;
    ProgressBar progressBar = null;
    int questionCount;
    List<Integer> questionCountList = new ArrayList<Integer>();
    TextView subject_name;
    CollapsingToolbarLayout collapsingToolbarLayout;
    TopicAdapterDB topicAdapterDB;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        progressBar = findViewById(R.id.progressbar);
        mService = Common.getAPI();

        topic_menu = findViewById(R.id.topic_menu);
        topic_menu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        topic_menu.setHasFixedSize(true);

        topicAdapterDB = new TopicAdapterDB(this, new ArrayList<classtipTopic>(), questionCountList);
        topic_menu.setAdapter(topicAdapterDB);

        subjectName = Common.currentClasstipSubject.subject_name;
        subjectId = Common.currentClasstipSubject.subject_id;
        chapterName = Common.currentClasstipChapter.chapter_name;
        subject_name = findViewById(R.id.subject_name);
        subject_name.setText(subjectName + "\n" + chapterName);

        collapsingToolbarLayout = findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        collapsingToolbarLayout.setTitle(chapterName);

        try {
            classmodeType = Common.currentClasstipClassMode.flag;
        } catch (NullPointerException e) {
            classmodeType = "v1";
        }

        if (classmodeType.equals("v1") || classmodeType.equals("q1")) {
            classmodeId = "1";
        } else
            classmodeId = Common.currentClasstipClassMode.class_mode_id;

        loadTopicDB(Common.currentClasstipChapter.chapter_id);

        if (isConnectingToInternet()) {
            progressBar.setVisibility(View.VISIBLE);
            loadTopicList(Common.currentClasstipChapter.chapter_id);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void loadTopicDB(String chapter_id) {
        compositeDisposable.add(Common.topicRepository.getTopicsById(Integer.parseInt(chapter_id), classmodeId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipTopic>>() {
                    @Override
                    public void accept(List<classtipTopic> classtipTopics) throws Exception {

                        try {

                            if(classtipTopics.size()>0) {


                                JSONArray req = new JSONArray(new Gson().toJson(classtipTopics));
                                for (int i = 0; i < req.length(); ++i) {
                                    JSONObject rec = req.getJSONObject(i);
                                    String id = String.valueOf(rec.getInt("topic_id"));
//                                    questionCount = Common.questionRepository.countQuestions(Integer.parseInt(id));
//                                    questionCountList.add(questionCount);

                                    if (Common.topicRepository.isUpdated("N", Integer.parseInt(id)) != 1) {
                                        Common.topicRepository.updateTopicsById(subjectId, subjectName, chapterName, "N", Integer.parseInt(id));
                                    }
                                }
                            }
                            else {

                                if(!DownloadUtils.isConnectingToInternet(TopicActivity.this)) {
                                    DownloadUtils.showCheckConnectiondialog(TopicActivity.this);

                                }


                            }

                        } catch (Exception e) {
                        }
                        topicAdapterDB.setTopicList(classtipTopics, questionCountList);
                    }
                }));
    }

    private void loadTopicList(final String chapId) {


      Call<JsonArray>jsonArrayCall= mService.getTopicById(chapId, classmodeId);
      jsonArrayCall.enqueue(new Callback<JsonArray>() {
          @Override
          public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {

              progressBar.setVisibility(View.GONE);

              List<classtipTopic>topiclist=new ArrayList();


              try {
                  JSONArray req = new JSONArray(response.body().toString());
                  for (int i = 0; i < req.length(); ++i) {
                      JSONObject rec = req.getJSONObject(i);
                      String id = String.valueOf(rec.getInt("topic_id"));
                      String topicName = rec.getString("topic_name");
                      String chapId = rec.getString("chptr_id");
                      String priority = rec.getString("priority");
                      String topicType = rec.getString("topic_type");
                      String classmodeId = rec.getString("classmode_id");
                      String video_id=rec.getString("video_id");
                      String url=rec.getString("url");
                      String video_name=rec.getString("video_name");
                      String youtube_id=rec.getString("youtube_id");
                      String video_type=rec.getString("video_type");
                      String questioncount=rec.getString("questioncount");
                      JSONArray jsonArray=rec.getJSONArray("question");
//                                JSONArray jsonArray=null;
//
                      for (int j=0;j<jsonArray.length();j++)
                      {

                          JSONObject jsonObject=jsonArray.getJSONObject(j);

                          classtipQuestion classtipQues=new GsonBuilder().create().fromJson(jsonObject.toString(),classtipQuestion.class);



                          if(questionRepository.isExist(Integer.parseInt(classtipQues.question_id))!=1)
                          {
                              questionRepository.insertQuestions(classtipQues);
                          }
                          else {

                              questionRepository.updateQuestions(classtipQues);
                          }


                      }


//                                questionCount = questionRepository.countQuestions(Integer.parseInt(id));
//                                questionCountList.add(questionCount);

                      if (Common.topicRepository.isExist(Integer.parseInt(id)) != 1) {
                          classtipTopic topic = new classtipTopic();
                          topic.topic_id = id;
                          topic.topic_name = topicName;
                          topic.subId = subjectId;
                          topic.subName = subjectName;
                          topic.chptr_id = chapId;
                          topic.chapName = chapterName;
                          topic.priority = priority;
                          topic.updtn_flag = "Y";
                          topic.viewFlag = "N";
                          topic.topic_type = topicType;
                          topic.classmode_id = classmodeId;
                          topic.video_id=video_id;
                          topic.url=url;
                          topic.video_name=video_name;
                          topic.youtube_id=youtube_id;
                          topic.video_type=video_type;
                          topic.questioncount=questioncount;
                          // topic.question=jsonArray.toString();

                          topiclist.add(topic);


                          Common.topicRepository.insertTopics(topic);
                          Toast.makeText(TopicActivity.this, new Gson().toJson(Common.topicRepository.countTopics()), Toast.LENGTH_SHORT).show();
                          Log.d("classtip23", new Gson().toJson(topic));
                      } else if (Common.topicRepository.isUpdated("N", Integer.parseInt(id)) != 1) {
                          Common.topicRepository.updateTopicsById(subjectId, subjectName, chapterName, "N", Integer.parseInt(id));
                      }

                  }

              } catch (Exception e) {
                  e.printStackTrace();

                  Log.e("Topic err",e.toString());
              }


              // classtipQuestion classtipQues=new GsonBuilder().create().fromJson(jsonObject.toString(),classtipQuestion.class);

              TypeToken<List<classtipTopic>> token = new TypeToken<List<classtipTopic>>() {};
              List<classtipTopic>userItems= new GsonBuilder().create().fromJson(response.body().toString(),token.getType());



              topicAdapterDB.setTopicList(userItems, questionCountList);

          }

          @Override
          public void onFailure(Call<JsonArray> call, Throwable t) {

              progressBar.setVisibility(View.GONE);
              if(!DownloadUtils.isConnectingToInternet(TopicActivity.this)) {
                  DownloadUtils.showCheckConnectiondialog(TopicActivity.this);

              }
              else {
                  Toast.makeText(TopicActivity.this, "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();

              }
          }
      });






//        compositeDisposable.add(mService.getTopicById(chapId, classmodeId)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .onExceptionResumeNext(new Observable<JSONArray>() {
//                    @Override
//                    protected void subscribeActual(Observer<? super JSONArray> observer) {
//                    }
//                })
//                .doOnError(new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//
//
//                    }
//                })
//                .doOnEach(new Consumer<Notification<JSONArray>>() {
//                    @Override
//                    public void accept(Notification<JSONArray> listNotification) throws Exception {
//
//                    }
//                })
//                .subscribe(new Consumer<JSONArray>() {
//                    @Override
//                    public void accept(JSONArray topics) throws Exception {
//                        List<classtipTopic>topiclist=new ArrayList();
//
//
//                        try {
//                            JSONArray req = topics;
//                            for (int i = 0; i < req.length(); ++i) {
//                                JSONObject rec = req.getJSONObject(i);
//                                String id = String.valueOf(rec.getInt("topic_id"));
//                                String topicName = rec.getString("topic_name");
//                                String chapId = rec.getString("chptr_id");
//                                String priority = rec.getString("priority");
//                                String topicType = rec.getString("topic_type");
//                                String classmodeId = rec.getString("classmode_id");
//                                String video_id=rec.getString("video_id");
//                                String url=rec.getString("url");
//                                String video_name=rec.getString("video_name");
//                                String youtube_id=rec.getString("youtube_id");
//                                String video_type=rec.getString("video_type");
//                                String questioncount=rec.getString("questioncount");
//                                JSONArray jsonArray=rec.getJSONArray("question");
////                                JSONArray jsonArray=null;
////
//                                for (int j=0;j<jsonArray.length();j++)
//                                {
//
//                                    JSONObject jsonObject=jsonArray.getJSONObject(j);
//
//                                    classtipQuestion classtipQues=new GsonBuilder().create().fromJson(jsonObject.toString(),classtipQuestion.class);
//
//                                    //currentClasstipQuestion currentClasstipQuestion
//
//
//
//                                        questionRepository.insertQuestions(classtipQues);
//
//
//                                }
//
//
////                                questionCount = questionRepository.countQuestions(Integer.parseInt(id));
////                                questionCountList.add(questionCount);
//
//                                if (Common.topicRepository.isExist(Integer.parseInt(id)) != 1) {
//                                    classtipTopic topic = new classtipTopic();
//                                    topic.topic_id = id;
//                                    topic.topic_name = topicName;
//                                    topic.subId = subjectId;
//                                    topic.subName = subjectName;
//                                    topic.chptr_id = chapId;
//                                    topic.chapName = chapterName;
//                                    topic.priority = priority;
//                                    topic.updtn_flag = "Y";
//                                    topic.viewFlag = "N";
//                                    topic.topic_type = topicType;
//                                    topic.classmode_id = classmodeId;
//                                    topic.video_id=video_id;
//                                    topic.url=url;
//                                    topic.video_name=video_name;
//                                    topic.youtube_id=youtube_id;
//                                    topic.video_type=video_type;
//                                    topic.questioncount=questioncount;
//                                   // topic.question=jsonArray.toString();
//
//                                    topiclist.add(topic);
//
//
//                                    Common.topicRepository.insertTopics(topic);
//                                    Toast.makeText(TopicActivity.this, new Gson().toJson(Common.topicRepository.countTopics()), Toast.LENGTH_SHORT).show();
//                                    Log.d("classtip23", new Gson().toJson(topic));
//                                } else if (Common.topicRepository.isUpdated("N", Integer.parseInt(id)) != 1) {
//                                    Common.topicRepository.updateTopicsById(subjectId, subjectName, chapterName, "N", Integer.parseInt(id));
//                                }
//
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//
//                       // classtipQuestion classtipQues=new GsonBuilder().create().fromJson(jsonObject.toString(),classtipQuestion.class);
//
//                        topicAdapterDB.setTopicList(topiclist, questionCountList);
//                    }
//                }));
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        progressBar.setVisibility(View.VISIBLE);
        loadTopicList(Common.currentClasstipChapter.chapter_id);

    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("test", "Permission is granted");
                return true;
            } else {

                Log.v("test", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("test", "Permission is granted");
            return true;
        }
    }

}
