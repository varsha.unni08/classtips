package com.classtips.liju.classtips.Model;

/**
 * Created by User on 10/10/2018.
 */

public class QuestionVideo {
    public String questionvideo ;
    public String questionid ;
    public String examid ;
    public String classmodeid ;
    public String topicid ;
    public String question ;
    public String questiontype ;
    public String orderno ;

    public QuestionVideo() {
    }

    public String getQuestionvideo() {
        return questionvideo;
    }

    public void setQuestionvideo(String questionvideo) {
        this.questionvideo = questionvideo;
    }

    public String getQuestionid() {
        return questionid;
    }

    public void setQuestionid(String questionid) {
        this.questionid = questionid;
    }

    public String getExamid() {
        return examid;
    }

    public void setExamid(String examid) {
        this.examid = examid;
    }

    public String getClassmodeid() {
        return classmodeid;
    }

    public void setClassmodeid(String classmodeid) {
        this.classmodeid = classmodeid;
    }

    public String getTopicid() {
        return topicid;
    }

    public void setTopicid(String topicid) {
        this.topicid = topicid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestiontype() {
        return questiontype;
    }

    public void setQuestiontype(String questiontype) {
        this.questiontype = questiontype;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }
}
