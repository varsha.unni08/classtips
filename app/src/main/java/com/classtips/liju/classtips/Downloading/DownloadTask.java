package com.classtips.liju.classtips.Downloading;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;
import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by User on 9/20/2018.
 */

public class DownloadTask {

    private String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String dataPath = SDPath + "/classtip/" ;
    private String zipPath = SDPath + "/classtip/" ;
    private String unzipPath = SDPath + "/classtip/" ;

    private static final String TAG = "Download Task";
    private Context context;
    private TextView textDownload;
    // Progress Dialog
    private ProgressDialog pDialog;
    // Progress dialog type (0 - for Horizontal progress bar)
    private static final int progress_bar_type = 0;
    private String downloadUrl = "", downloadFileName = "";

    CompositeDisposable compositeDisposable=new CompositeDisposable();

    public DownloadTask(Context context, TextView textDownload, String downloadUrl,ProgressDialog pDialog) {
        this.context = context;
        this.textDownload = textDownload;
        this.downloadUrl = downloadUrl;
        this.pDialog=pDialog;

        downloadFileName = downloadUrl.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
        Log.e(TAG, downloadFileName);

        //Start Downloading Task
        new DownloadingTask().execute();
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            textDownload.setEnabled(false);
            textDownload.setText("Download Started");//Set Button Text when download started
            onCreateDialog(progress_bar_type);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = c.getContentLength();
                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (new CheckForSDCard().isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + Utils.downloadDirectory);
                } else
                    Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                long total = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    textDownload.setEnabled(true);
                    textDownload.setText("completed");//If Download completed then change button text
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory+"/");

                    if (unzip(uri + downloadFileName,unzipPath)) {
                        if (Common.downloadRepository.isFlgExist("N",Integer.parseInt(Common.currentDownload.download_id))==1){

                            int id= Integer.parseInt(Common.currentDownload.download_id);
                            String downloadFlag="Y";
                            Common.downloadRepository.updateDownloads(downloadFlag,id);

                        }
                        pDialog.dismiss();
                        Toast.makeText(context,"Downloading Completed.",Toast.LENGTH_LONG).show();
                        File file = new File(String.valueOf(outputFile));
                        boolean deleted = file.delete();
                    }
                    getVideos();
                    getTopicQuestions();
                    getYearWiseQuestions();

                } else {
                    textDownload.setText("failed");//If download failed change button text
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            textDownload.setEnabled(true);
                            textDownload.setText("again");//Change button text again after 3sec
                        }
                    }, 3000);
                    pDialog.dismiss();
                    Log.e(TAG, "Download Failed");
                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs
                pDialog.dismiss();
                textDownload.setText("failed");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        textDownload.setEnabled(true);
                        textDownload.setText("again");
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }

        private void getVideos() {
            compositeDisposable.add(Common.recentRepository.getRecentVideos()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<classtipRecent>>() {
                        @Override
                        public void accept(List<classtipRecent> classtipRecents) throws Exception {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipRecents));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                String url = rec.getString("url");
                                String downloadFileName = rec.getString("videoName");
                                Toast.makeText(context, "id:"+url, Toast.LENGTH_SHORT).show();
//                                        String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                        + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                                if (downloadFileName.indexOf(".") > 0)
                                    downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                                Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                        + "/" + Utils.downloadDirectory + "/" + downloadFileName);

                                File file = new File(String.valueOf(uri));
                                if (file.exists()) {
//                                            Toast.makeText(context, "exists", Toast.LENGTH_SHORT).show();
                                    encrypt(uri,ur);
                                    boolean deleted = file.delete();

                                }else {
//                                            Toast.makeText(context, "not", Toast.LENGTH_SHORT).show();
                                }

                                Log.d("new", String.valueOf(uri));

                                Log.d("test", downloadFileName);

                            }
                        }
                    }));
        }

        private void getTopicQuestions() {
            compositeDisposable.add(Common.questionRepository.getQuestions()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<classtipQuestion>>() {
                        @Override
                        public void accept(List<classtipQuestion> classtipQuestions) throws Exception {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipQuestions));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                String url = rec.getString("questionVideo");
                                Toast.makeText(context, "id:"+url, Toast.LENGTH_SHORT).show();
                                String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                        + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                                if (downloadFileName.indexOf(".") > 0)
                                    downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                                Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                        + "/" + Utils.downloadDirectory + "/" + downloadFileName);

                                File file = new File(String.valueOf(uri));
                                if (file.exists()) {
                                    encrypt(uri,ur);
                                    boolean deleted = file.delete();

                                }else {

                                }

                                Log.d("test", downloadFileName);

                            }
                        }
                    }));
        }

        private void getYearWiseQuestions() {
            compositeDisposable.add(Common.questionYearRepository.getQuestions()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnComplete(new Action() {
                        @Override
                        public void run() throws Exception {
                            pDialog.dismiss();
                        }
                    })
                    .subscribe(new Consumer<List<classtipQuestionYear>>() {
                        @Override
                        public void accept(List<classtipQuestionYear> classtipQuestionYears) throws Exception {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipQuestionYears));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                String url = rec.getString("questionVideo");
                                Toast.makeText(context, "id:"+url, Toast.LENGTH_SHORT).show();
                                String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                                Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                        + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                                if (downloadFileName.indexOf(".") > 0)
                                    downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                                Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                        + "/" + Utils.downloadDirectory + "/" + downloadFileName);

                                File file = new File(String.valueOf(uri));
                                if (file.exists()) {
                                    encrypt(uri,ur);
                                    boolean deleted = file.delete();

                                }else {

                                }

                                Log.d("test", downloadFileName);

                            }
                        }
                    }));
        }
        /**
         * Updating progress bar
         * */
        private void publishProgress(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(context);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(false);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    public static Boolean unzip(String sourceFile, String destinationFolder)  {
        ZipInputStream zis = null;

        try {
            zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(sourceFile)));
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                String fileName = ze.getName();
                fileName = fileName.substring(fileName.indexOf("/") + 1);
                File file = new File(destinationFolder, fileName);
                File dir = ze.isDirectory() ? file : file.getParentFile();

                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Invalid path: " + dir.getAbsolutePath());
                if (ze.isDirectory()) continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }

            }
        } catch (IOException ioe){
            Log.d(TAG,ioe.getMessage());
            return false;
        }  finally {
            if(zis!=null)
                try {
                    zis.close();
                } catch(IOException e) {

                }
        }
        return true;
    }

    private boolean encrypt(Uri uri, Uri ur) {
        try {

            byte[] fileData = FileUtils.readFile(String.valueOf(uri));
            byte[] encodedBytes = EncryptDecryptUtils.encode(EncryptDecryptUtils.getInstance(context).getSecretKey(), fileData);
            FileUtils.saveFile(encodedBytes, String.valueOf(ur));
            return true;
        } catch (Exception e) {

        }
        return false;
    }
}
