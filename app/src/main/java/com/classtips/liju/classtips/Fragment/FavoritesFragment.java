package com.classtips.liju.classtips.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.classtips.liju.classtips.Database.AdapterDB.FavoriteAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class FavoritesFragment extends Fragment {

    private Unbinder unbinder;

    RecyclerView recyclerFav;
    CompositeDisposable compositeDisposable;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_favorite, container, false);
        unbinder= ButterKnife.bind(this,view);

        compositeDisposable=new CompositeDisposable();
        recyclerFav=(RecyclerView)view.findViewById(R.id.recycler_favorite);
        recyclerFav.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        recyclerFav.setHasFixedSize(true);

        loadFavorites();

        return view;

    }

    private void loadFavorites() {

        compositeDisposable.add(Common.topicRepository.getFavoritessById()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipTopic>>() {
                    @Override
                    public void accept(List<classtipTopic> classtipTopics) throws Exception {
                        displayFavorites(classtipTopics);
                    }
                }));

    }

    private void displayFavorites(List<classtipTopic> classtipTopics) {

        FavoriteAdapterDB adapter=new FavoriteAdapterDB(getActivity(),classtipTopics);
        recyclerFav.setAdapter(adapter);

    }

}
