package com.classtips.liju.classtips.Downloading;

import android.os.Environment;

/**
 * Created by User on 9/20/2018.
 */

public class CheckForSDCard {

    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
}
