package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/28/2018.
 */

public class classtipQuestionYearRepository implements IclasstipQuestionYearDataSource {

    private IclasstipQuestionYearDataSource iclasstipQuestionYearDataSource;
    private static classtipQuestionYearRepository instance;

    public classtipQuestionYearRepository(IclasstipQuestionYearDataSource iclasstipQuestionYearDataSource) {
        this.iclasstipQuestionYearDataSource = iclasstipQuestionYearDataSource;
    }

    public static classtipQuestionYearRepository getInstance(IclasstipQuestionYearDataSource iclasstipQuestionYearDataSource){
        if (instance==null)
            instance=new classtipQuestionYearRepository(iclasstipQuestionYearDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipQuestionYear>> getQuestions() {
        return iclasstipQuestionYearDataSource.getQuestions();
    }

    @Override
    public Flowable<List<classtipQuestionYear>> getQuestionsById(int examID) {
        return iclasstipQuestionYearDataSource.getQuestionsById(examID);
    }

    @Override
    public int isExist(int questionId) {
        return iclasstipQuestionYearDataSource.isExist(questionId);
    }

    @Override
    public int countQuestions() {
        return iclasstipQuestionYearDataSource.countQuestions();
    }

    @Override
    public void insertQuestions(classtipQuestionYear... classtipQuestions) {
        iclasstipQuestionYearDataSource.insertQuestions(classtipQuestions);
    }

    @Override
    public void updateQuestions(classtipQuestionYear... classtipQuestionYears) {
        iclasstipQuestionYearDataSource.updateQuestions(classtipQuestionYears);
    }
}
