package com.classtips.liju.classtips.Utils;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.classtips.liju.classtips.Adapter.CartAdapter;

/**
 * Created by User on 10/3/2018.
 */

public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {

    RecyclerItemTouchHelperListener listener;

    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener=listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if (listener!=null)
            listener.onSwiped(viewHolder,direction,viewHolder.getAdapterPosition());
    }

    //Ctrl+O


    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        View forgroundView=((CartAdapter.CartViewHolder)viewHolder).view_forground;
        getDefaultUIUtil().clearView(forgroundView);
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder!=null) {
            View forgroundView = ((CartAdapter.CartViewHolder) viewHolder).view_forground;
            getDefaultUIUtil().onSelected(forgroundView);
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View forgroundView = ((CartAdapter.CartViewHolder) viewHolder).view_forground;
        getDefaultUIUtil().onDraw(c,recyclerView,forgroundView,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View forgroundView = ((CartAdapter.CartViewHolder) viewHolder).view_forground;
        getDefaultUIUtil().onDrawOver(c, recyclerView,forgroundView, dX, dY, actionState, isCurrentlyActive);
    }
}
