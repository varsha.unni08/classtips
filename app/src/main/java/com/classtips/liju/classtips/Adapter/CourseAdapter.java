package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.MainActivity;
import com.classtips.liju.classtips.Model.Course;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;

import java.util.List;

/**
 * Created by User on 8/17/2018.
 */

public class CourseAdapter extends
        RecyclerView.Adapter<CourseAdapter.ViewHolder> {

    private List<Course> courses;
    private Context context;

    private int lastSelectedPosition = -1;

    public CourseAdapter(Context context, List<Course> courses) {
        this.context = context;
        this.courses = courses;
    }

    @Override
    public CourseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.course_list_layout, parent, false);

        CourseAdapter.ViewHolder viewHolder = new CourseAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CourseAdapter.ViewHolder holder, final int position) {
        //OffersModel offersModel = offersList.get(position);
        holder.selectionState.setText(courses.get(position).course_name);
        //holder.offerAmount.setText("" + offersModel.getSavings());

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections

        holder.selectionState.setChecked(lastSelectedPosition == position);
        holder.selectionState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
                Toast.makeText(CourseAdapter.this.context, "selected course is " + holder.selectionState.getText(), Toast.LENGTH_LONG).show();
                Common.currentCourse= courses.get(position);
                //context.startActivity(new Intent(context,MainActivity.class));
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("key","1");
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView courseName ;
        //public TextView offerAmount;
        public RadioButton selectionState;

        public ViewHolder(View view) {
            super(view);
            //courseName = (TextView) view.findViewById(R.id.course_name);
            //offerAmount = (TextView) view.findViewById(R.id.offer_amount);
            selectionState = (RadioButton) view.findViewById(R.id.radio_course);


        }
    }
}