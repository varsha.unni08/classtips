package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/5/2018.
 */
@Dao
public interface classtipRecentDAO {

    @Query("SELECT * FROM classtipRecent")
    Flowable<List<classtipRecent>> getRecentVideos();

    @Query("SELECT * FROM classtipRecent where topicId=:topicId")
    public List<classtipRecent> getRecentVideosById(int topicId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipRecent where id=:videoId)")
    int isExist(int videoId);

    @Query("SELECT COUNT(*) FROM classtipRecent")
    int countVideos();

    @Query("Delete  from classtipRecent")
    void  emptyVideos();

    @Insert
    void insertVideos(classtipRecent...classtipRecents);

    @Update
    void updateVideos(classtipRecent...classtipRecents);

    @Delete
    void deleteVideos(classtipRecent...classtipRecents);
}
