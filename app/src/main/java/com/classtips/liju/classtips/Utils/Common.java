package com.classtips.liju.classtips.Utils;

import android.os.Build;

import com.classtips.liju.classtips.Database.DataSource.classtipChapterRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipClassModeRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipDownloadRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipExamRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipPurchaseRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipQuestionRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipQuestionYearRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipRecentRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipSubjectRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipTopicRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipUserRepository;
import com.classtips.liju.classtips.Database.Local.ClasstipDatabase;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;
import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;
import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.ChapterTopic;
import com.classtips.liju.classtips.Model.ClassMode;
import com.classtips.liju.classtips.Model.Course;
import com.classtips.liju.classtips.Model.DownloadList;
import com.classtips.liju.classtips.Model.Exams;
import com.classtips.liju.classtips.Model.QRcode;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.Model.QuestionYear;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.Model.Topic;
import com.classtips.liju.classtips.Model.User;
import com.classtips.liju.classtips.Model.Video;
import com.classtips.liju.classtips.Model.Wallet;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LIJU on 8/4/2018.
 */

public class Common {
    public static final String BASE_URL="https://classtips.in/classtip/";
    public static final String BASE_URLM="http://classtips.in/classtip/";
//    private static final String BASE_URL="https://classtip.in/classtip/";
//    private static final String BASE_URL="https://169.254.80.58/classtip/";

    public static final String key="JaNdRgUkXp2s5v8y/B?E(G+KbPeShVmY";






    public static User currentUser=null;
    public static Subject currentSubject=null;
    public static ChapterTopic currentChapter=null;
    public static Course currentCourse=null;
    public static Topic currentTopic=null;
    public static ClassMode currentClassmode=null;
    public static Video currentVideo=null;
    public static Question currentQuestion=null;
    public static QuestionYear currentQuestionYear=null;
    public static Exams currentExam=null;
    public static Wallet currentWallet=null;
    public static QRcode currentQRcode=null;
    public static DownloadList currentDownload=null;

    //Database
    public static ClasstipDatabase classtipDatabase;
    public static classtipRecentRepository recentRepository;
    public static classtipSubjectRepository subjectRepository;
    public static classtipChapterRepository chapterRepository;
    public static classtipTopicRepository topicRepository;
    public static classtipUserRepository userRepository;
    public static classtipPurchaseRepository purchaseRepository;
    public static classtipClassModeRepository classModeRepository;
    public static classtipQuestionRepository  questionRepository;
    public static classtipQuestionYearRepository questionYearRepository;
    public static classtipExamRepository examRepository;
    public static classtipDownloadRepository downloadRepository;


    //Database current values
    public static classtipUser currentClasstipUser=null;
    public static classtipChapter currentClasstipChapter=null;
    public static classtipTopic currentClasstipTopic=null;
    public static classtipRecent currentClasstipRecent=null;
    public static classtipSubject currentClasstipSubject=null;
    public static classtipClassMode currentClasstipClassMode=null;
    public static classtipQuestion currentClasstipQuestion=null;
    public static classtipQuestionYear currentClasstipQuestionYear=null;
    public static classtipPurchase currentClasstipPurchase=null;

    public static int currenttopicposition=0;
    public static int totaltopicsize=0;

    public static List<classtipTopic>curr_classtipTopics=new ArrayList<>();


    public static IClassTipAPI getAPI(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return RetrofitClient.getClient(BASE_URL).create(IClassTipAPI.class);
        }else
            return RetrofitClient.getClient(BASE_URLM).create(IClassTipAPI.class);
    }
}
