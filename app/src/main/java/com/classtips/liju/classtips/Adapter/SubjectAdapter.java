package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.ChapterActivity;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 8/9/2018.
 */

public class SubjectAdapter extends RecyclerView.Adapter<SubjectViewHolder> {
    Context context;
    List<Subject> subjects;

    public SubjectAdapter(Context context, List<Subject> subjects) {
        this.context = context;
        this.subjects = subjects;
    }

    @NonNull
    @Override
    public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.subj_menu_layout,null);
        return new SubjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubjectViewHolder holder, final int position) {
        //Load image
        Picasso.with(context)
                .load(subjects.get(position).link)
                .into(holder.image_product);
        holder.txt_subjct_name.setText(subjects.get(position).subject_name);

        //Event
        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                Common.currentSubject= subjects.get(position);
                Common.currentClassmode=null;
                Common.currentClasstipClassMode=null;
                Common.currentClasstipSubject=null;
                if (Common.subjectRepository.isExist(Integer.parseInt(subjects.get(position).subject_id))!=1)
                {
                    Toast.makeText(context, "subject added", Toast.LENGTH_SHORT).show();
                    classtipSubject classtipSubjects=new classtipSubject();
                    classtipSubjects.subject_id= subjects.get(position).subject_id;
                    classtipSubjects.subject_name=subjects.get(position).subject_name;
                    classtipSubjects.course_id=subjects.get(position).course_id;
                    classtipSubjects.link=subjects.get(position).link;
                    classtipSubjects.prize=subjects.get(position).prize;

                    Common.subjectRepository.insertSubjects(classtipSubjects);
                    Toast.makeText(context, new Gson().toJson(Common.subjectRepository.countSubjects()), Toast.LENGTH_SHORT).show();
                    Log.d("classtip23", new Gson().toJson(classtipSubjects));
                }
                else
                {

                }
                //Start new activity

                context.startActivity(new Intent(context,ChapterActivity.class));
//                context.startActivity(new Intent(context,ClassModeActivity.class));
            }
        });


    }



    @Override
    public int getItemCount() {
        return subjects.size();
    }
}
