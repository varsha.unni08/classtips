package com.classtips.liju.classtips;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.classtips.liju.classtips.Adapter.Classmode2Adapter;
import com.classtips.liju.classtips.Model.ClassMode;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ClassModeActivity extends AppCompatActivity {
    IClassTipAPI mService;
    RecyclerView class_menu;
    //RxJava
    CompositeDisposable compositeDisposable=new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_mode);

        mService= Common.getAPI();

        class_menu=(RecyclerView)findViewById(R.id.class_menu);
        class_menu.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        class_menu.setHasFixedSize(true);

        getClassMode();
    }

    private void getClassMode() {
//        compositeDisposable.add(mService.getClassmodes()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<List<ClassMode>>() {
//                    @Override
//                    public void accept(List<ClassMode> classModes) throws Exception {
//                        displayClassMode(classModes);
//                    }
//                }));
    }

    private void displayClassMode(List<ClassMode> classModes) {
        Classmode2Adapter adapter=new Classmode2Adapter(this,classModes);
        class_menu.setAdapter(adapter);
    }
}
