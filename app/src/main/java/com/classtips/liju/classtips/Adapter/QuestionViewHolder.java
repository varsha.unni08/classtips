package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by User on 8/28/2018.
 */

public class QuestionViewHolder extends RecyclerView.ViewHolder  {
    public ImageView image_product,imgdownload;
    public TextView text_watch;
    public TextView text_topic_name;
    public TextView text_answer_img;

    public QuestionViewHolder(View itemView) {
        super(itemView);
        image_product=itemView.findViewById(R.id.image_product);
//        image_product=itemView.findViewById(R.id.myZoomageView);
        text_watch=itemView.findViewById(R.id.text_watch);
        text_topic_name=itemView.findViewById(R.id.txt_question_name);
        text_answer_img=itemView.findViewById(R.id.text_answer_img);
        imgdownload=itemView.findViewById(R.id.imgdownload);
    }

}
