package com.classtips.liju.classtips.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;

import java.util.List;

/**
 * Created by User on 9/15/2018.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    Context context;
    List<classtipPurchase> purchases;

    public CartAdapter(Context context, List<classtipPurchase> purchases) {
        this.context = context;
        this.purchases = purchases;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteView= LayoutInflater.from(context).inflate(R.layout.cart_item_layout,parent,false);
        return new CartViewHolder(iteView);
    }

    @Override
    public void onBindViewHolder(CartViewHolder holder, final int position) {
        holder.txt_subject_name.setText(purchases.get(position).subjectName);
        holder.text_price.setText(new StringBuilder("₹").append(purchases.get(position).price));
        holder.cart_delete_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Delete item fromRoom Database
                Common.purchaseRepository.deletePurchases(purchases.get(position));
                Toast.makeText(context, purchases.get(position).subjectName+" is removed from your cart", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder{
        TextView txt_subject_name,text_price;
        ImageView cart_delete_icon;

        public RelativeLayout view_background;
        public RelativeLayout view_forground;

        public CartViewHolder(View itemView) {
            super(itemView);
            txt_subject_name=(TextView)itemView.findViewById(R.id.txt_subjct_name);
            text_price=(TextView)itemView.findViewById(R.id.txt_price);
            cart_delete_icon=(ImageView)itemView.findViewById(R.id.cart_delete_icon);

            view_background=(RelativeLayout)itemView.findViewById(R.id.view_background);
            view_forground=(RelativeLayout) itemView.findViewById(R.id.view_forground);
        }
    }

    public void removeItem(int position){
        purchases.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(classtipPurchase item,int position){
        purchases.add(position,item);
        notifyItemInserted(position);
    }
}
