package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipUser;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/12/2018.
 */

public interface IclasstipUserDataSource {

    Flowable<List<classtipUser>> getUsers();

    Flowable<List<classtipUser>> getUsersById(int courseId);

    int isExist(int phone);

    int isUpdated(String flag,String phone);

    void updateUser(String flag, String phone);

    int countUsers();

    void insertUsers(classtipUser...classtipUsers);

    void updateUserDate(String date, String phone);

    void updateExpiryFlag(String flag, String phone);
}
