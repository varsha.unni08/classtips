package com.classtips.liju.classtips.Model;

/**
 * Created by User on 8/31/2018.
 */

public class Purchase {
    private String courseid;
    private String subid;
    private String phone;
    private String comment;

    public Purchase() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCourseid() {
        return courseid;
    }

    public void setCourseid(String courseid) {
        this.courseid = courseid;
    }

    public String getSubid() {
        return subid;
    }

    public void setSubid(String subid) {
        this.subid = subid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
