package com.classtips.liju.classtips;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Downloading.EncryptDecryptUtils;
import com.classtips.liju.classtips.Downloading.FileUtils;
import com.classtips.liju.classtips.Downloading.Utils;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Model.UserComment;
import com.classtips.liju.classtips.Model.Video;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Retrofit.RetrofitClient;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.DownloadUtils;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoHomeActivity extends AppCompatActivity implements Handler.Callback, YouTubePlayer.OnInitializedListener, YouTubePlayer.PlaybackEventListener {
    IClassTipAPI mService;
    VideoView videoView;
    RecyclerView more_details_video;
    String phone="", subject_id, topic_id, name, topicType, chapId;
    private int position = 0, getPosition = 0, priority = 0;
    Runnable runnable;
    Boolean expandState = false;
    Button btn_question, btn_next_topic, btn_submit;
    TextView text;
    Toast toast;
    EditText comment;
    private static ImageButton downloadVideo;

     File file_offlineplay=null;

    ImageView pausevideo;
    private MediaController mediaController;
    ProgressBar progressBar = null;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    YouTubePlayerFragment youtubeFragment;

    ProgressBar progressBar2;

    YouTubePlayer youTubePlayer = null;

    LinearLayout layout_downloaded,layout_youtube;

    int downloadID = 0;

    Handler handler;



    private final String API_KEY = "AIzaSyCb4JlXATybV1BL5mYCTY3Cd_Rp1quwD2Q";
    private final String VIDEO_CODE = "HXG9WcmySME";

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.v("test", "Permission: " + permissions[0] + "was " + grantResults[0]);
            //resume tasks needing this permission
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_home);
        youtubeFragment = (YouTubePlayerFragment)
                getFragmentManager().findFragmentById(R.id.youtubeFragment);
        layout_youtube=findViewById(R.id.layout_youtube);
        handler = new Handler();

        videoView = findViewById(R.id.videoView);
        mService = Common.getAPI();
        progressBar = findViewById(R.id.progressbar);
        progressBar2 = findViewById(R.id.progressBar2);
        pausevideo = findViewById(R.id.pausevideo);
        layout_downloaded = findViewById(R.id.layout_downloaded);

        more_details_video = findViewById(R.id.recycler_more_details_video);
        more_details_video.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        more_details_video.setHasFixedSize(true);

        //Custom Toast Layout
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));
        text = layout.findViewById(R.id.text);
        toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);

        compositeDisposable.add(Common.userRepository.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipUser>>() {
                    @Override
                    public void accept(List<classtipUser> classtipUsers) throws Exception {
                        Log.d("classtip234", new Gson().toJson(classtipUsers));
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                phone = rec.getString("phone");
                                name = rec.getString("name");
                            }
                            playVideo(phone);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }));

        final ExpandableRelativeLayout expandableLayout
                = findViewById(R.id.expandableLayout);
        final RelativeLayout expandableView = findViewById(R.id.video_clik_expand);
        final View expandView = findViewById(R.id.video_view);

        expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {

            @Override
            public void onPreOpen() {

                changeRotate(expandableView, 0f, 180f).start();
                expandState = true;
            }

            @Override
            public void onPreClose() {
                changeRotate(expandableView, 180f, 0f).start();
                expandState = false;
            }


        });
        expandableView.setRotation(expandState ? 180f : 0f);
        expandableView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableLayout.toggle();
            }
        });

        btn_next_topic = findViewById(R.id.btn_next_topic);
        btn_question = findViewById(R.id.btn_topic);
        btn_submit = findViewById(R.id.btn_submit);
        comment = findViewById(R.id.text_comment);
        downloadVideo = findViewById(R.id.downloadVideo);

        downloadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                downloadVideo();
            }
        });
        // downloadVideo.setVisibility(View.GONE);

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            lanscapeConfigurationListener();
        try {
            priority = Integer.parseInt(Common.currentTopic.priority) + 1;
        } catch (Exception e) {
            priority = Integer.parseInt(Common.currentClasstipTopic.priority) + 1;
        }
        try {
            chapId = Common.currentTopic.chptr_id;
        } catch (Exception e) {
            chapId = Common.currentClasstipTopic.chptr_id;
        }
        btn_next_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressBar.setVisibility(View.VISIBLE);


                if(videoView.isPlaying())
                {
                    videoView.stopPlayback();
                }

                int p=Common.currenttopicposition;

                if(p==Common.totaltopicsize-1)
                {
                    Toast.makeText(VideoHomeActivity.this,"You covered all topics in this chapter",Toast.LENGTH_SHORT).show();

             onBackPressed();

                }
                else {

                    p++;

                    Common.currentClasstipTopic=  Common.curr_classtipTopics.get(p);
                    Common.currenttopicposition=p;
                    playVideo(phone);
                }



//                if (isConnectingToInternet()) {
//                    mService.getNextVideos(chapId, String.valueOf(priority))
//                            .enqueue(new Callback<Video>() {
//                                @Override
//                                public void onResponse(Call<Video> call, Response<Video> response) {
//                                    priority++;
//                                    Video nextVideo = response.body();
//                                    if (nextVideo.isError()) {
//                                        Toast.makeText(VideoHomeActivity.this, "No more topics in this section", Toast.LENGTH_SHORT).show();
//                                    } else {
//                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                            videoView.setVideoPath(nextVideo.url);
//                                        } else
//                                            videoView.setVideoPath(Common.BASE_URLM + "/" + Common.currentClasstipSubject.subject_name + "/" + Common.currentClasstipChapter.priority + "/" + nextVideo.getVideoName());
//                                    }
//
//                                }
//
//                                @Override
//                                public void onFailure(Call<Video> call, Throwable t) {
//
//                                }
//                            });
//                }

            }
        });
        btn_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                Intent intent = new Intent(VideoHomeActivity.this, QuestionActivity.class);
                startActivity(intent);
                finish();
            }
        });
        //Method for playing video
        isStoragePermissionGranted();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }
                final Handler handler = new Handler();
                runnable = new Runnable() {
                    public void run() {
                        int duration = videoView.getCurrentPosition();
                        if (getPosition == duration && videoView.isPlaying()) {
                            progressBar.setVisibility(View.VISIBLE);
                        } else {
                            progressBar.setVisibility(View.GONE);
                        }
                        getPosition = duration;

                        handler.postDelayed(runnable, 1000);
                    }
                };
                handler.postDelayed(runnable, 0);

                mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        if (isConnectingToInternet()) {
                            text.setText("Network Error!!!");
                            toast.show();
                        } else {
                            text.setText("You are not connected to the internet");
                            toast.show();
                        }
                        return false;
                    }
                });

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        if (mediaController == null) {
                            mediaController = new MediaController(VideoHomeActivity.this);
                            mediaController.setAlpha(0.5f);
                            videoView.setMediaController(mediaController);
                        }
                        mediaController.setAnchorView(videoView);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });

        pausevideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(VideoHomeActivity.this);
                //builder.setTitle("Class Tip").setSingleChoiceItems()

                // add a list
                String[] animals = {"Resume", "cancel"};
                builder.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:


                                PRDownloader.resume(downloadID);


                                break;
                            case 1:

                                PRDownloader.cancel(downloadID);


                                break;

                        }
                    }
                });        // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


        progressBar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(VideoHomeActivity.this);
                //builder.setTitle("Class Tip").setSingleChoiceItems()

                // add a list
                String[] animals = {"pause", "cancel"};
                builder.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:


                                PRDownloader.pause(downloadID);


                                break;
                            case 1:


                                PRDownloader.cancel(downloadID);


                                break;

                        }
                    }
                });        // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

        if (Common.currentVideo != null) {




                this.youTubePlayer = youTubePlayer;
                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                youTubePlayer.loadVideo(Common.currentClasstipTopic.youtube_id.trim());

                youTubePlayer.play();





        }


    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }


    @Override
    public void onPlaying() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int i) {

    }


    public void downloadVideo() {


        if(isConnectingToInternet()){





       // if (Common.currentVideo != null) {


          //  File f = new File(getExternalCacheDir().getPath(), Common.currentVideo.videoName);


            String url = DownloadUtils.PHYSICS_TOPICURL+Common.currentClasstipTopic.chptr_id+"/"+Common.currentClasstipTopic.topic_id+"/"+Common.currentClasstipTopic.video_name;



            final File file_dir=new File(getExternalCacheDir()+"/"+Common.currentClasstipSubject.subject_id+"/"+Common.currentClasstipTopic.chptr_id+"/"+Common.currentClasstipTopic.topic_id);


            if(!file_dir.exists())
            {
                file_dir.mkdirs();

            }

           // if (!f.exists()) {


              //  String url = "https://classtips.in/classtip/PHYSICS/1/" + Common.currentVideo.videoName;


                PRDownloaderConfig config1 = PRDownloaderConfig.newBuilder()
                        .setReadTimeout(30_000)
                        .setConnectTimeout(30_000)
                        .build();
                PRDownloader.initialize(VideoHomeActivity.this, config1);


                downloadID = PRDownloader.download(url, file_dir.getAbsolutePath(), Common.currentClasstipTopic.video_name)
                        .build()
                        .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                            @Override
                            public void onStartOrResume() {

                                // Toast.makeText(VideoHomeActivity.this, "Resumed", Toast.LENGTH_SHORT).show();

                                layout_downloaded.setVisibility(View.GONE);
                                progressBar2.setVisibility(View.VISIBLE);
                                downloadVideo.setVisibility(View.GONE);
                                pausevideo.setVisibility(View.GONE);

                            }
                        })
                        .setOnPauseListener(new OnPauseListener() {
                            @Override
                            public void onPause() {

                                progressBar2.setVisibility(View.GONE);
                                downloadVideo.setVisibility(View.GONE);
                                pausevideo.setVisibility(View.VISIBLE);
                                layout_downloaded.setVisibility(View.GONE);


                                Toast.makeText(VideoHomeActivity.this, "paused", Toast.LENGTH_SHORT).show();


                            }
                        })
                        .setOnCancelListener(new OnCancelListener() {
                            @Override
                            public void onCancel() {

                                progressBar2.setVisibility(View.GONE);
                                downloadVideo.setVisibility(View.VISIBLE);
                                pausevideo.setVisibility(View.GONE);
                                layout_downloaded.setVisibility(View.GONE);


                                Toast.makeText(VideoHomeActivity.this, "cancelled", Toast.LENGTH_SHORT).show();


                            }
                        })
                        .setOnProgressListener(new OnProgressListener() {
                            @Override
                            public void onProgress(Progress progress) {


                                long totalbytes = progress.totalBytes;
                                long currentbytes = progress.currentBytes;

                                double tot = Double.parseDouble(String.valueOf(totalbytes));
                                double curr = Double.parseDouble(String.valueOf(currentbytes));


                                double d = curr / tot;
                                double prg = d * 100;

                                final int p = (int) prg;

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //while (pStatus <= 100) {
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressBar2.setProgress(p);
                                                //txtProgress.setText(pStatus + " %");
                                            }
                                        });
                                        try {
                                            Thread.sleep(100);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        // pStatus++;
                                        // }
                                    }
                                }).start();


                                // progressBar.setProgress(p);


                            }
                        }).start(new OnDownloadListener() {
                            @Override
                            public void onDownloadComplete() {

                                progressBar2.setVisibility(View.GONE);
                                downloadVideo.setVisibility(View.GONE);
                                pausevideo.setVisibility(View.GONE);
                                layout_downloaded.setVisibility(View.VISIBLE);

                                Toast.makeText(VideoHomeActivity.this, "downloaded", Toast.LENGTH_SHORT).show();
                                File f_download = new File(file_dir.getAbsolutePath(), Common.currentClasstipTopic.video_name);


                                DownloadUtils.splitvideo(f_download);


                                if(f_download.exists())
                                {
                                    f_download.delete();
                                }

                                playvideoOffline();





                            }

                            @Override
                            public void onError(Error error) {

                                progressBar2.setVisibility(View.GONE);
                                downloadVideo.setVisibility(View.VISIBLE);
                                pausevideo.setVisibility(View.GONE);
                                layout_downloaded.setVisibility(View.GONE);

                                if(isConnectingToInternet()){


                                    Toast.makeText(VideoHomeActivity.this, "Error while download", Toast.LENGTH_SHORT).show();


                                }
                                else {

                                    Toast.makeText(VideoHomeActivity.this, "please check internet", Toast.LENGTH_SHORT).show();


                                }
                            }
                        });

//            } else {
//
//                Toast.makeText(VideoHomeActivity.this, "already downloaded", Toast.LENGTH_SHORT).show();
//
//            }


       // }

        }
        else {

            Toast.makeText(VideoHomeActivity.this, "please check internet", Toast.LENGTH_SHORT).show();


        }
    }





    public void showDownloadAlert() {

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this);

        builder.setMessage("To watch this video properly you must download the video.Do you want to download video ?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        downloadVideo();

                        dialog.dismiss();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();

                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("ClassTips");
        alert.show();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // unregisterReceiver(onDownloadComplete);


        if(file_offlineplay!=null)
        {
            if(file_offlineplay.exists()) {

                file_offlineplay.delete();
            }
        }
    }




    private void lanscapeConfigurationListener() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        btn_submit.setVisibility(View.GONE);
        //btn_question.setVisibility(View.GONE);
        //  btn_next_topic.setVisibility(View.GONE);
        downloadVideo.setVisibility(View.GONE);
        comment.setVisibility(View.GONE);

        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    // TODO: The system bars are visible. Make any desired
                    // adjustments to your UI, such as showing the action bar or
                    // other navigational controls.

                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_IMMERSIVE
                                    // Set the content to appear under the system bars so that the
                                    // content doesn't resize when the system bars hide and show.
                                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    // Hide the nav bar and status bar
//                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN);

                    Handler h = new Handler();

                    h.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            decorView.setSystemUiVisibility(
                                    View.SYSTEM_UI_FLAG_IMMERSIVE
                                            // Set the content to appear under the system bars so that the
                                            // content doesn't resize when the system bars hide and show.
                                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                            // Hide the nav bar and status bar
                                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
                        }
                    }, 5000);


                } else {
                    // TODO: The system bars are NOT visible. Make any desired
                    // adjustments to your UI, such as hiding the action bar or
                    // other navigational controls.
                }
            }
        });
    }

    private ObjectAnimator changeRotate(RelativeLayout expandView, float to, float from) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(expandView, "rotation", to, from);
        animator.setDuration(300);
        animator.setInterpolator(com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_INTERPOLATOR));
        return animator;
    }

    private void playVideo(String phone) {

        //  videourl==https://classtips.in/classtip/PHYSICS/1/+2.Ker.Phy.Cha1.T1.1-1.2.mp4

       // if (isConnectingToInternet()) {
            try {
                subject_id = Common.currentClasstipTopic.subId;
            } catch (Exception e) {
                subject_id = Common.currentClasstipSubject.subject_id;
            }
            try {
                topic_id = Common.currentTopic.topic_id;
            } catch (NullPointerException e) {
                topic_id = Common.currentClasstipTopic.topic_id;
            }
            try {
                topicType = Common.currentTopic.topic_type;
            } catch (NullPointerException e) {
                topicType = Common.currentClasstipTopic.topic_type;
            }



            if(Common.currentClasstipSubject.ispurchased) {

               // Toast.makeText(VideoHomeActivity.this, "You  purchased this subject", Toast.LENGTH_SHORT).show();



                File file_dir = new File(getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id);


                //  File f = new File(getExternalCacheDir().getPath(), Common.currentVideo.videoName);

                if (DownloadUtils.getFilelist(file_dir).size() == 0) {

                    youtubeFragment.initialize(API_KEY, VideoHomeActivity.this
                    );
                    showDownloadAlert();


                } else {

                    playvideoOffline();

                    progressBar2.setVisibility(View.GONE);
                    downloadVideo.setVisibility(View.GONE);
                    pausevideo.setVisibility(View.GONE);
                    layout_downloaded.setVisibility(View.VISIBLE);


                }
            }
            else {
               // Toast.makeText(VideoHomeActivity.this, "You have not purchased this subject", Toast.LENGTH_SHORT).show();




                if(Common.currentClasstipTopic.video_type.equals("F")) {

                    File file_dir = new File(getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id);


                    //  File f = new File(getExternalCacheDir().getPath(), Common.currentVideo.videoName);

                    if (DownloadUtils.getFilelist(file_dir).size() == 0) {

                        youtubeFragment.initialize(API_KEY, VideoHomeActivity.this
                        );
                        showDownloadAlert();


                    } else {

                        playvideoOffline();

                        progressBar2.setVisibility(View.GONE);
                        downloadVideo.setVisibility(View.GONE);
                        pausevideo.setVisibility(View.GONE);
                        layout_downloaded.setVisibility(View.VISIBLE);


                    }
                }
                else{


                    Toast.makeText(VideoHomeActivity.this, "please purchase the subject", Toast.LENGTH_SHORT).show();

                }







            }



//            mService.checkPurchaseExists(phone, subject_id)
//                    .enqueue(new Callback<CheckPurchaseResponse>() {
//                        @Override
//                        public void onResponse(Call<CheckPurchaseResponse> call, Response<CheckPurchaseResponse> response) {
//                            CheckPurchaseResponse purchaseResponse = response.body();
//                            if (purchaseResponse.isExists()) {
//                                //Toast.makeText(VideoHomeActivity.this, "exists", Toast.LENGTH_SHORT).show();
//
//
//
//
//
//
//
//
//
//
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<CheckPurchaseResponse> call, Throwable t) {
//
//                        }
//                    });

//        } else {
            //Common.recentRepository.getRecentVideosById(Integer.parseInt(Common.currentClasstipTopic.id));

//            try {
//
//                topic_id = Common.currentClasstipTopic.topic_id;
//            } catch (NullPointerException e) {
//                topic_id = Common.currentTopic.topic_id;
//            }
//
//
//            if(Common.currentClasstipTopic.video_type.equals("F")) {
//
//                File file_dir = new File(getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id);
//
//
//                //  File f = new File(getExternalCacheDir().getPath(), Common.currentVideo.videoName);
//
//                if (DownloadUtils.getFilelist(file_dir).size() == 0) {
//
//                    youtubeFragment.initialize(API_KEY, VideoHomeActivity.this
//                    );
//                    showDownloadAlert();
//
//
//                } else {
//
//                    playvideoOffline();
//
//                    progressBar2.setVisibility(View.GONE);
//                    downloadVideo.setVisibility(View.GONE);
//                    pausevideo.setVisibility(View.GONE);
//                    layout_downloaded.setVisibility(View.VISIBLE);
//
//
//                }
//            }


           // classtipRecent video = new classtipRecent();
            // Toast.makeText(this,new Gson().toJson(Common.recentRepository.getRecentVideosById(Integer.parseInt(Common.currentTopic.topic_id))), Toast.LENGTH_SHORT).show();
            //String recs = new Gson().toJson(Common.recentRepository.getRecentVideosById(Integer.parseInt(Common.currentTopic.topic_id)));

//            try {
//                JSONArray req = new JSONArray(new Gson().toJson(Common.recentRepository.getRecentVideosById(Integer.parseInt(topic_id))));
//                for (int i = 0; i < req.length(); ++i) {
//                    JSONObject rec = req.getJSONObject(i);
//                    String downloadFileName = rec.getString("videoName");
//                    String url = rec.getString("url");
////                        String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
//                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
//                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
//                    if (downloadFileName.indexOf(".") > 0)
//                        downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
//                    Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
//                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
//                    try {
//                        playAudio(FileUtils.getTempFileDescriptor(this, decrypt(ur, uri)));
//                    } catch (IOException | NullPointerException e) {
//                        text.setText("You yet havent download this video");
//                        toast.show();
//                        e.printStackTrace();
//                    }
//                    Log.e("test", String.valueOf(ur));
//
//                }
//            } catch (JSONException e) {
//
//                e.printStackTrace();
//            }
     //   }

    }




    public void playvideoOffline()
    {

        try {
            File file_dir = new File(getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id);


            //  File f = new File(getExternalCacheDir().getPath(), Common.currentVideo.videoName);


            List<File>list=DownloadUtils.getFilelist(file_dir);
            if (list.size() > 0) {

       file_offlineplay = new File(file_dir.getAbsolutePath(), Common.currentClasstipTopic.video_name);


       new FileMergeAsync(list,file_offlineplay).execute("");












            }

        }catch (Exception e)
        {

        }

        }




    private class FileMergeAsync extends AsyncTask<String,String,File>
    {

        List<File>files;

        File file_into;

        public FileMergeAsync(List<File> files, File file_into) {
            this.files = files;
            this.file_into = file_into;
        }

        @Override
        protected File doInBackground(String... strings) {

            try {

                if(!file_into.exists())
                {

                    file_into.createNewFile();
                }

                BufferedInputStream in=null;

                FileOutputStream fos = new FileOutputStream(file_into);
                BufferedOutputStream mergingStream = new BufferedOutputStream(fos) ;
                for(File f :files)

                {


                    FileInputStream fileInputStream=new FileInputStream(f);
                    in=new BufferedInputStream(fileInputStream);
                    byte[] buf = new byte[1024*1024];
                    int len;

                    while ((len = in.read(buf)) > 0) {
                        mergingStream.write(buf, 0, len);
                    }



                    //mergingStream.write();
                }

                in.close();
                mergingStream.close();
            }
            catch (Exception e)
            {
                Log.e("Tagggggg",e.toString());
            }


            return file_into;
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);


            file_offlineplay=file;

            layout_youtube.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);


            Uri uri = Uri.parse(file_offlineplay.getAbsolutePath());
            videoView.setVideoURI(uri);


            MediaController mediaController=new MediaController(VideoHomeActivity.this);
            mediaController.setAnchorView(videoView);

            videoView.setMediaController(mediaController);
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {



                    file_offlineplay.delete();

                }
            });


            videoView.start();

        }
    }










//    private void getMoreDetailedVieos(String topic_id) {
//        compositeDisposable.add(mService.getMoreVideos(topic_id)
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new Consumer<List<MoreDetailVideo>>() {
//                                @Override
//                                public void accept(List<MoreDetailVideo> moreDetailVideos) throws Exception {
//                                    displayMoreDetailedList(moreDetailVideos);
//                                }
//                            }));
//    }

//    private void displayMoreDetailedList(List<MoreDetailVideo> moreDetailVideos) {
//        MoreDetailedVideoAdapter adapter=new MoreDetailedVideoAdapter(this,moreDetailVideos);
//        more_details_video.setAdapter(adapter);
//    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) videoView.getLayoutParams();
        final View decorView = getWindow().getDecorView();
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            lanscapeConfigurationListener();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            //Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();

            btn_submit.setVisibility(View.VISIBLE);
            btn_question.setVisibility(View.VISIBLE);
            btn_next_topic.setVisibility(View.VISIBLE);
            downloadVideo.setVisibility(View.GONE);
            comment.setVisibility(View.VISIBLE);
//            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    } else {
                    }
                }
            });

            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_VISIBLE);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
        }
        videoView.setLayoutParams(params);


    }

    private byte[] decrypt(Uri ur, Uri uri) {
        try {
//            Toast.makeText(this, "decrypting", Toast.LENGTH_SHORT).show();
            byte[] fileData = FileUtils.readFile(String.valueOf(ur));
            byte[] decryptedBytes = EncryptDecryptUtils.decode(EncryptDecryptUtils.getInstance(this).getSecretKey(), fileData);
//            FileUtils.saveFile(decryptedBytes, String.valueOf(uri));
            return decryptedBytes;
        } catch (Exception e) {
        }
        return null;
    }


    private void playAudio(File fileDescriptor) {
        if (null == fileDescriptor) {
            return;
        }
        play(fileDescriptor);
    }

    public void play(final File fileDescriptor) {

        try {
            Uri video = Uri.parse(String.valueOf(fileDescriptor));
            Toast.makeText(VideoHomeActivity.this, String.valueOf(fileDescriptor), Toast.LENGTH_SHORT).show();
            videoView.setVideoURI(video);
            videoView.start();

        } catch (Exception e) {
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("test", "Permission is granted");
                return true;
            } else {

                Log.v("test", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("test", "Permission is granted");
            return true;
        }
    }

    private void showUpdateDialogue() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(VideoHomeActivity.this);
        builder.setTitle("Purchae Subject");
        View itemView = LayoutInflater.from(VideoHomeActivity.this)
                .inflate(R.layout.purchase_layout, null);

        builder.setView(itemView);

        builder.setMessage("You have to purchase subject for accessing this video class.Do you want to purchase?")
                .setCancelable(false)
                .setPositiveButton("PURCHASE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(VideoHomeActivity.this, PurchaseActivity.class);
                        //intent.putExtra("key","2");
                        startActivity(intent);
                        finish();
                    }
                }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    @Override
    public void onBackPressed() {
//            startActivity(new Intent(VideoHomeActivity.this, TopicActivity.class));
        finish();
    }

    @Override
    public void onPause() {
        Log.d("TAG", "onPause called");
        super.onPause();
        position = videoView.getCurrentPosition(); //stopPosition is an int
        videoView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TAG", "onResume called");
        videoView.seekTo(position);
        videoView.start(); //Or use resume() if it doesn't work. I'm not sure
    }

    @Override
    public boolean handleMessage(Message msg) {
        return false;
    }
}
