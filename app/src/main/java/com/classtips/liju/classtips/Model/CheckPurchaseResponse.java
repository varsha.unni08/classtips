package com.classtips.liju.classtips.Model;

/**
 * Created by User on 9/1/2018.
 */

public class CheckPurchaseResponse {
    private boolean exists;
    private String error_msg;

    public CheckPurchaseResponse() {
    }

    public boolean isExists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
