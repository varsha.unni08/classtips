package com.classtips.liju.classtips;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.CourseSpinnerAdapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.CheckUserResponse;
import com.classtips.liju.classtips.Model.Course;
import com.classtips.liju.classtips.Model.UpdateFlag;
import com.classtips.liju.classtips.Model.User;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.config.AppPreferences;
import com.classtips.liju.classtips.config.ApplicationConstants;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

import dmax.dialog.SpotsDialog;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, ApplicationConstants {

    private static final int REQUEST_CODE = 1000;
    String PhoneModel = Build.MANUFACTURER + ":" + android.os.Build.MODEL + ":" + android.os.Build.VERSION.RELEASE;
    Button btn_continue;
    IClassTipAPI mService;
    String deviceId ;
    String flag = "Y", course_id, course_expiry;
    TextView text;
    Toast toast;
    SharedPreferences sharedpreferences;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    AppPreferences appPreferences;
    AuthUI.IdpConfig config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mService = Common.getAPI();
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) findViewById(R.id.custom_toast_container));
        deviceId = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        config = new AuthUI.IdpConfig.PhoneBuilder().build();
        appPreferences = AppPreferences.getInstance(MainActivity.this, APPLICATION_DATA);
        text = layout.findViewById(R.id.text);
        toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        btn_continue = findViewById(R.id.btn_continue);

        startLoginPage();
//        btn_continue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        if (appPreferences.getBoolean(LOGGED_STATUS)) {
            final AlertDialog alertDialog = new SpotsDialog(MainActivity.this);
            alertDialog.show();
            alertDialog.setMessage("Please wait.....");
            alertDialog.setCancelable(false);
            btn_continue.setVisibility(View.GONE);
            final String phone = appPreferences.getString(PHONE);
            if (isConnectingToInternet()) {
                        mService.checkUserExists(phone)
                                .enqueue(new Callback<CheckUserResponse>() {
                                    @Override
                                    public void onResponse(Call<CheckUserResponse> call, Response<CheckUserResponse> response) {
                                        CheckUserResponse userResponse = response.body();
                                        if (userResponse.isExists()) {
                                            mService.getUserInformation(phone)
                                                    .enqueue(new Callback<User>() {
                                                        @Override
                                                        public void onResponse(Call<User> call, Response<User> response) {
                                                            alertDialog.dismiss();
                                                            Common.currentUser = response.body();
                                                            if (Common.currentUser.getImei().equals(deviceId)) {
                                                                mService.updateDate(Common.currentUser.getPhone())
                                                                        .enqueue(new Callback<UpdateFlag>() {
                                                                            @Override
                                                                            public void onResponse(Call<UpdateFlag> call, Response<UpdateFlag> response) {
                                                                                UpdateFlag updateFlag = response.body();
                                                                                if (updateFlag.isDateupdated()) {
                                                                                    homeActivity(phone);
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<UpdateFlag> call, Throwable t) {

                                                                            }
                                                                        });
                                                            } else {
                                                                text.setText("You are already logged in another device");
                                                                toast.show();
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<User> call, Throwable t) {

                                                            Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                                                        }
                                                    });

                                        } else {
                                            //else,need regidter
                                            alertDialog.dismiss();
                                            showRegisterDialog(phone, deviceId);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CheckUserResponse> call, Throwable t) {

                                    }
                                });
                    }

             else {
                alertDialog.dismiss();
                text.setText("No Internet");
                toast.show();
            }

        }
    }

    private void homeActivity(String phonrNumber) {
        mService.getUserInformation(phonrNumber)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        Common.currentUser = response.body();
                        if (!response.body().getImei().equals(deviceId)){
                            Toast.makeText(MainActivity.this, "You are already logged in another device", Toast.LENGTH_SHORT).show();
                        }else {
                            classtipUser user = new classtipUser();
                            user.phone = Common.currentUser.getPhone();
                            user.name = Common.currentUser.getName();
                            user.address = Common.currentUser.getAddress();
                            user.birthDate = Common.currentUser.getBirthdate();
                            user.courseId = Common.currentUser.getCourse();
                            user.updtnFlag = flag;
                            user.expiryFlag = Common.currentUser.getExpiry_flag();
                            user.instlnDate = String.valueOf(Common.currentUser.getApp_running_date());
                            user.expiryDate = String.valueOf(Common.currentUser.getExpiry_date());

                            Common.userRepository.insertUsers(user);
                            Log.d("classtip23", new Gson().toJson(user));

                            startActivity(new Intent(MainActivity.this, HomeActivity.class));
                            appPreferences.saveBoolean(LOGGED_STATUS, true);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
    }
    private void startLoginPage() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View register_layout = inflater.inflate(R.layout.mobile_register_layout, null);

        final MaterialEditText edt_phone = register_layout.findViewById(R.id.edt_phone);
        Button btn_register = register_layout.findViewById(R.id.btn_register);
        builder.setView(register_layout);
        final AlertDialog dialog = builder.create();
        final String finalCountryCode = "+91";
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!edt_phone.getText().toString().equals("")) {

                    if(edt_phone.getText().toString().length()==10) {

                    String phone = edt_phone.getText().toString();
                    phone = finalCountryCode + phone;
                    final String finalPhone = phone;
                    mService.checkUserExists(finalPhone)
                            .enqueue(new Callback<CheckUserResponse>() {

                                @Override
                                public void onResponse(Call<CheckUserResponse> call, Response<CheckUserResponse> response) {
                                    CheckUserResponse userResponse = response.body();
                                    dialog.dismiss();
                                    if (userResponse.isExists()) {
                                        homeActivity(finalPhone);
                                        appPreferences.saveString(PHONE, finalPhone);
                                    } else {


                                        showRegisterDialog(finalPhone, deviceId);
                                    }
                                }

                                @Override
                                public void onFailure(Call<CheckUserResponse> call, Throwable t) {

                                    dialog.dismiss();
                                    Log.e("TAG", t.toString());


                                }
                            });
                    }
                    else {

                        Toast.makeText(MainActivity.this,"Enter a valid mobile number",Toast.LENGTH_SHORT).show();

                    }
                }
                else {

                    Toast.makeText(MainActivity.this,"Enter the mobile number",Toast.LENGTH_SHORT).show();

                }
            }
        });
        dialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (response.getError() != null) {
                Toast.makeText(this, "" + response.getError().getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                    final AlertDialog alertDialog = new SpotsDialog(MainActivity.this);
                    alertDialog.show();
                    alertDialog.setMessage("Please wait.....");
                    alertDialog.setCancelable(false);

                    mService.checkUserExists(user.getPhoneNumber().toString())
                            .enqueue(new Callback<CheckUserResponse>() {

                                @Override
                                public void onResponse(Call<CheckUserResponse> call, Response<CheckUserResponse> response) {
                                    CheckUserResponse userResponse = response.body();
                                    if (userResponse.isExists()) {
                                        homeActivity(user.getPhoneNumber().toString());
                                        alertDialog.dismiss();
                                    } else {
                                        alertDialog.dismiss();
                                        showRegisterDialog(user.getPhoneNumber().toString(), deviceId);
                                    }
                                }

                                @Override
                                public void onFailure(Call<CheckUserResponse> call, Throwable t) {

                                }
                            });
                }
                        else
                        Toast.makeText(this, "User does not exist", Toast.LENGTH_SHORT).show();


            }
        }
        }
    }

    private void showRegisterDialog(final String phone, final String deviceId) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("REGISTER");
        LayoutInflater inflater = this.getLayoutInflater();
        View register_layout = inflater.inflate(R.layout.register_layout, null);

        final MaterialEditText edt_name = register_layout.findViewById(R.id.edt_name);
        final MaterialEditText edt_address = register_layout.findViewById(R.id.edt_address);
        final MaterialEditText edt_birthdate = register_layout.findViewById(R.id.edt_birthdate);
        final Spinner spinner_course =  register_layout.findViewById(R.id.spinner_course);
        spinner_course.setOnItemSelectedListener(this);

        compositeDisposable.add(mService.getCourses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Course>>() {
                    @Override
                    public void accept(List<Course> courses) throws Exception {
                        CourseSpinnerAdapter adapter = new CourseSpinnerAdapter(MainActivity.this,
                                R.layout.spinner_course_list, courses);
                        spinner_course.setAdapter(adapter);
                    }
                }));

        Button btn_register = register_layout.findViewById(R.id.btn_register);

        edt_birthdate.addTextChangedListener(new PatternedTextWatcher("####-##-##"));

        builder.setView(register_layout);
        final AlertDialog dialog = builder.create();
        //Event
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                dialog.dismiss();
                if (TextUtils.isEmpty(edt_address.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Please enter your address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(edt_birthdate.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Please enter your birthdate", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(edt_name.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();
                    return;
                }

                final AlertDialog waitingDialog = new SpotsDialog(MainActivity.this);
                waitingDialog.show();
                waitingDialog.setMessage("Please waiting");
                mService.registerNewUser(phone,
                        edt_name.getText().toString(),
                        edt_address.getText().toString(),
                        edt_birthdate.getText().toString(), course_id, course_expiry, deviceId, PhoneModel, flag)
                        //course type to add
                        .enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                waitingDialog.dismiss();
                                dialog.dismiss();
                                User user = response.body();
                                if (TextUtils.isEmpty(user.getError_msg())) {
                                    // Toast.makeText(MainActivity.this, "User register successfully", Toast.LENGTH_SHORT).show();
                                    Common.currentUser = response.body();

                                    classtipUser userData = new classtipUser();
                                    userData.phone = Common.currentUser.getPhone();
                                    userData.name = Common.currentUser.getName();
                                    userData.address = Common.currentUser.getAddress();
                                    userData.birthDate = Common.currentUser.getBirthdate();
                                    userData.courseId = Common.currentUser.getCourse();
                                    userData.updtnFlag = flag;
                                    userData.expiryFlag = Common.currentUser.getExpiry_flag();
                                    userData.instlnDate = String.valueOf(Common.currentUser.getApp_running_date());
                                    userData.expiryDate = String.valueOf(Common.currentUser.getExpiry_date());

                                    Common.userRepository.insertUsers(userData);

                                    startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                    appPreferences.saveBoolean(LOGGED_STATUS, true);
                                    appPreferences.saveString(PHONE,phone);
                                    finish();
                                }


                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                waitingDialog.dismiss();
                            }
                        });
            }


        });
        dialog.show();
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    //Exit application when clicked back button
    Boolean isBackButtonClicked = false;

    @Override
    public void onBackPressed() {
        if (isBackButtonClicked) {
            super.onBackPressed();
            return;
        }
        this.isBackButtonClicked = true;
        Toast.makeText(this, "Please click BACK again to exit from Classtip", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        isBackButtonClicked = false;
        super.onResume();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        course_id = ((TextView) view.findViewById(R.id.txt_id)).getText().toString();
        course_expiry = ((TextView) view.findViewById(R.id.txt_expiry)).getText().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}