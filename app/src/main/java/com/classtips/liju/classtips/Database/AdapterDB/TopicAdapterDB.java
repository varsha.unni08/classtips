package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.TopicViewHolder;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.QuestionActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.TopicActivity;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.DownloadUtils;
import com.classtips.liju.classtips.Utils.ProgressDialog;
import com.classtips.liju.classtips.VideoHomeActivity;
import com.classtips.liju.classtips.config.ApplicationConstants;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by User on 9/11/2018.
 */

public class TopicAdapterDB extends RecyclerView.Adapter<TopicViewHolder> {
    Context context;
    List<classtipTopic> classtipTopics;
    List<Integer> questionCount;

    Handler handler;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public TopicAdapterDB(Context context, List<classtipTopic> classtipTopics, List<Integer> questionCountList) {
        this.context = context;
        this.classtipTopics = classtipTopics;
        this.questionCount = questionCountList;
        handler=new Handler();
    }

    @Override
    public TopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.topic_item_layout, null);
        return new TopicViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TopicViewHolder holder, final int position) {
        holder.text_topic_name.setText(classtipTopics.get(position).topic_name);
        holder.questiom_count.setText("No of questions " + classtipTopics.get(position).questioncount);


        if(!classtipTopics.get(position).video_name.equals("")) {


           // File file_todownload = new File(context.getExternalCacheDir(), classtipTopics.get(position).video_name);


            File file_dir=new File(context.getExternalCacheDir()+"/"+Common.currentClasstipSubject.subject_id+"/"+classtipTopics.get(position).chptr_id+"/"+classtipTopics.get(position).topic_id);





            if (DownloadUtils.getFilelist(file_dir).size()>0) {


             //   if(Common.currentClasstipSubject.ispurchased) {


                    holder.image_download.setVisibility(View.GONE);

                    holder.progressBar2.setVisibility(View.GONE);
                    holder.image_pause.setVisibility(View.GONE);
                    holder.layout_downloaded.setVisibility(View.VISIBLE);
            //    }


            }

            else if (classtipTopics.get(position).downloadID != -1 && classtipTopics.get(position).isdownloading && !classtipTopics.get(position).ispaused) {
                holder.image_download.setVisibility(View.GONE);
                holder.progressBar2.setVisibility(View.VISIBLE);
                holder.image_pause.setVisibility(View.GONE);
                holder.layout_downloaded.setVisibility(View.GONE);
            }

          else  if (classtipTopics.get(position).downloadID != -1 && classtipTopics.get(position).isdownloading && classtipTopics.get(position).ispaused) {

                holder.image_download.setVisibility(View.GONE);
                holder.image_pause.setVisibility(View.VISIBLE);

                holder.progressBar2.setVisibility(View.GONE);
                holder.layout_downloaded.setVisibility(View.GONE);


            }


          else {

                holder.image_download.setVisibility(View.VISIBLE);
                holder.image_pause.setVisibility(View.GONE);
                holder.progressBar2.setVisibility(View.GONE);
                holder.layout_downloaded.setVisibility(View.GONE);
            }










        }



        holder.image_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(classtipTopics.get(position).downloadID!=-1&&classtipTopics.get(position).isdownloading&&classtipTopics.get(position).ispaused)
                {

                    //PRDownloader.pause(classtipTopics.get(position).downloadID);




                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    //builder.setTitle("Class Tip").setSingleChoiceItems()

                    // add a list
                    String[] animals = {"Resume", "cancel"};
                    builder.setItems(animals, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:

                                    PRDownloader.resume(classtipTopics.get(position).downloadID);


                                    break;
                                case 1:

                                    PRDownloader.cancel(classtipTopics.get(position).downloadID);


                                    break;

                            }
                        }
                    });        // create and show the alert dialog
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }






            }
        });



        holder.progressBar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(classtipTopics.get(position).downloadID!=-1&&classtipTopics.get(position).isdownloading&&!classtipTopics.get(position).ispaused)
                {

                    //PRDownloader.pause(classtipTopics.get(position).downloadID);




                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    //builder.setTitle("Class Tip").setSingleChoiceItems()

                    // add a list
                    String[] animals = {"pause", "cancel"};
                    builder.setItems(animals, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:

                                    PRDownloader.pause(classtipTopics.get(position).downloadID);


                                    break;
                                case 1:

                                    PRDownloader.cancel(classtipTopics.get(position).downloadID);


                                    break;

                            }
                        }
                    });        // create and show the alert dialog
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }





            }
        });




        if (Common.topicRepository.isViewed("N", Integer.parseInt(classtipTopics.get(position).topic_id)) == 1)
            holder.cardView.setCardBackgroundColor(Color.WHITE);
        else
            holder.cardView.setCardBackgroundColor(Color.CYAN);


        holder.image_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(classtipTopics.get(position).video_type.equals("F")) {


                    if (((TopicActivity) context).isStoragePermissionGranted()) {


                        classtipTopics.get(position).isdownloading = true;


                        String url = DownloadUtils.PHYSICS_TOPICURL + classtipTopics.get(position).chptr_id + "/" + classtipTopics.get(position).topic_id + "/" + classtipTopics.get(position).video_name;

                        Log.e("Downloadurl", url);

                        try {

                            File file_dir = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + classtipTopics.get(position).chptr_id + "/" + classtipTopics.get(position).topic_id);


                            if (!file_dir.exists()) {
                                file_dir.mkdirs();

                            }


                            Toast.makeText(context,"Downloading topic video",Toast.LENGTH_SHORT).show();

                            downloadFile(url, file_dir.getAbsolutePath(), classtipTopics.get(position).video_name, holder, position);



                            compositeDisposable.add(Common.questionRepository.getQuestionsById(Integer.parseInt(classtipTopics.get(position).topic_id))
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(new Consumer<List<classtipQuestion>>() {
                                                   @Override
                                                   public void accept(List<classtipQuestion> classtipQuestions) throws Exception {

                                                       try{


                                                           File file_dir=null;
                                                           String url="";

                                                           JSONArray jsonArray = new JSONArray(new Gson().toJson(classtipQuestions));



                                                           for (int i=0;i<jsonArray.length();i++) {



                                                               String question_id=jsonArray.getJSONObject(i).getString("question_id");

                                                               String question_img=jsonArray.getJSONObject(i).getString("question_img");

                                                               String answer_img=jsonArray.getJSONObject(i).getString("answer_img");

                                                               String question_name=jsonArray.getJSONObject(i).getString("question_name");


                                                               File file_dirq = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + classtipTopics.get(position).chptr_id + "/" + classtipTopics.get(position).topic_id + "/"+question_id);


                                                               if (!file_dirq.exists()) {
                                                                   file_dirq.mkdirs();

                                                               }


                                                               url = DownloadUtils.PHYSICS_TOPICURL + classtipTopics.get(position).chptr_id + "/" + classtipTopics.get(position).topic_id + "/" + ApplicationConstants.qvideo+"/"+question_name;

                                                               classtipTopics.get(position).isdownloading = true;
                                                               notifyItemRangeChanged(0, classtipTopics.size());

                                                               downloadFile(url, file_dirq.getAbsolutePath(), question_name, holder, position);






                                                               String fileName = question_img.substring(question_img.lastIndexOf('/') + 1);

                                                              // String timeStamp = "QUESTION"+String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

                                                               File ftodownload=new File(file_dirq,fileName);


                                                               new DownloadImage(question_img,ftodownload.getAbsolutePath()).execute("");



                                                               //String timeStam = "ANSWER"+String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));



                                                               String fileNameimg = question_img.substring(question_img.lastIndexOf('/') + 1);

                                                               // String timeStamp = "QUESTION"+String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

                                                              // File ftodownloadimg=new File(file_dirq,fileNameimg);

                                                               File ftodownloadanswer=new File(file_dirq,fileNameimg);


                                                               new DownloadImage(answer_img,ftodownloadanswer.getAbsolutePath()).execute("");


                                                           }



                                                       }catch (Exception e)
                                                       {
e.printStackTrace();
                                                       }



                                                   }
                                               }
                                    ));















                        } catch (Exception e) {

                        }

                    } else {

                        ((TopicActivity) context).isStoragePermissionGranted();
                    }

                }
                else {


                    if (Common.currentClasstipSubject.ispurchased) {

                        if (((TopicActivity) context).isStoragePermissionGranted()) {


                            classtipTopics.get(position).isdownloading = true;
                            notifyItemRangeChanged(0, classtipTopics.size());


                            String url = DownloadUtils.PHYSICS_TOPICURL + classtipTopics.get(position).chptr_id + "/" + classtipTopics.get(position).topic_id + "/" + classtipTopics.get(position).video_name;

                            Log.e("Downloadurl", url);

                            try {

                                File file_dir = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + classtipTopics.get(position).chptr_id + "/" + classtipTopics.get(position).topic_id);


                                if (!file_dir.exists()) {
                                    file_dir.mkdirs();

                                }


                                downloadFile(url, file_dir.getAbsolutePath(), classtipTopics.get(position).video_name, holder, position);


                            } catch (Exception e) {

                            }

                        } else {

                            ((TopicActivity) context).isStoragePermissionGranted();
                        }


                    }
                    else {

                        Toast.makeText(context, "please purchase the subject", Toast.LENGTH_SHORT).show();



                    }
                }




            }
        });

        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                Common.currentClasstipTopic = classtipTopics.get(position);
                Common.totaltopicsize=classtipTopics.size();
                Common.currenttopicposition=position;
                Common.curr_classtipTopics=classtipTopics;
                Common.currentTopic = null;
                try {
                    try {
                        if ("q1".equals(Common.currentClasstipClassMode.flag)) {
                            Intent intent = new Intent(context, QuestionActivity.class);
                            intent.putExtra("key", classtipTopics.get(position).video_name);
                            context.startActivity(intent);
                        } else
                            context.startActivity(new Intent(context, VideoHomeActivity.class));
                    } catch (NullPointerException e) {
                        if ("q1".equals(Common.currentClassmode.flag)) {
                            Intent intent = new Intent(context, QuestionActivity.class);
                            intent.putExtra("key", classtipTopics.get(position).video_name);
                            context.startActivity(intent);
                        } else


                            context.startActivity(new Intent(context, VideoHomeActivity.class));
                    }
                } catch (NullPointerException e) {
                    context.startActivity(new Intent(context, VideoHomeActivity.class));
                }



                if (Common.topicRepository.isViewed("N", Integer.parseInt(classtipTopics.get(position).topic_id)) == 1) {

                    int id = Integer.parseInt(classtipTopics.get(position).topic_id);
                    String viewFlag = "Y";
                    Common.topicRepository.updateTopics(viewFlag, id);

                }

            }
        });

        //Favorite system

        String classmodeType;
        try {
            classmodeType = Common.currentClasstipClassMode.flag;
        } catch (Exception e) {
            classmodeType = "v1";
        }

        if ("v1".equals(classmodeType)) {
            if (Common.topicRepository.isFavorite(Integer.parseInt(classtipTopics.get(position).topic_id)) == 1)
                holder.image_favorite.setImageResource(R.drawable.ic_favorites_filled_icon);
            else
                holder.image_favorite.setImageResource(R.drawable.ic_favorites_border_icon);

            holder.image_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Common.topicRepository.isFavorite(Integer.parseInt(classtipTopics.get(position).topic_id)) != 1) {
                        Common.topicRepository.updateFavorite(Integer.parseInt(classtipTopics.get(position).topic_id));
                        holder.image_favorite.setImageResource(R.drawable.ic_favorites_filled_icon);


                    } else {
                        Common.topicRepository.removeFavorite(Integer.parseInt(classtipTopics.get(position).topic_id));
                        holder.image_favorite.setImageResource(R.drawable.ic_favorites_border_icon);
                    }
                }
            });
        } else {
            holder.image_favorite.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return classtipTopics.size();
    }

    public void setTopicList(List<classtipTopic> classtipTopics, List<Integer> questionCountList) {
        this.classtipTopics = classtipTopics;
        this.questionCount = questionCountList;
        notifyDataSetChanged();
    }


    public void downloadFile(final String url, final String filepath, final String file, final TopicViewHolder topicViewHolder, final int pos) {
        PRDownloaderConfig config1 = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(context, config1);


        int downloadId = PRDownloader.download(url, filepath, file)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {

                        Toast.makeText(context, "Resumed", Toast.LENGTH_SHORT).show();

                       // classtipTopics.get(pos).isresumed=true;
                        classtipTopics.get(pos).ispaused=false;
                        notifyItemChanged(pos);


                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                        Toast.makeText(context, "paused", Toast.LENGTH_SHORT).show();


                       // classtipTopics.get(pos).isresumed=false;
                        classtipTopics.get(pos).ispaused=true;
                        notifyItemChanged(pos);


                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                        Toast.makeText(context, "cancelled", Toast.LENGTH_SHORT).show();

                        classtipTopics.get(pos).downloadID=-1;
                        classtipTopics.get(pos).isdownloading=false;
                       // classtipTopics.get(pos).isresumed=false;
                        classtipTopics.get(pos).ispaused=false;
                        notifyItemChanged(pos);


                        File file = new File(context.getExternalCacheDir(), classtipTopics.get(pos).video_name);

                        try {
                            if (file.exists()) {
                                file.delete();
                            }
                        }catch (Exception e)
                        {

                        }


                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {





                        long totalbytes=progress.totalBytes;
                        long currentbytes=progress.currentBytes;

                        double tot=Double.parseDouble(String.valueOf(totalbytes));
                        double curr=Double.parseDouble(String.valueOf(currentbytes));



                        double d=curr/tot;
                        double prg=d*100;

                        final    int p=(int)prg;

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //while (pStatus <= 100) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        topicViewHolder.progressBar2.setProgress(p);
                                        //txtProgress.setText(pStatus + " %");
                                    }
                                });
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                // pStatus++;
                                // }
                            }
                        }).start();


                    }
                }).start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {

                        try {

                            Toast.makeText(context, "downloaded", Toast.LENGTH_SHORT).show();

                            classtipTopics.get(pos).downloadID = -1;
                            classtipTopics.get(pos).isdownloading = false;
                            notifyItemChanged(pos);

                            File f_download = new File(filepath, file);


                            DownloadUtils.splitvideo(f_download);


                            if(f_download.exists())
                            {
                                f_download.delete();
                            }




                        }catch (Exception e)
                        {
                            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();

                        }



                    }

                    @Override
                    public void onError(Error error) {
                        Toast.makeText(context, "" + error, Toast.LENGTH_SHORT).show();


                        if(((TopicActivity)context).isConnectingToInternet()){


                            Toast.makeText(context, "Error while download", Toast.LENGTH_SHORT).show();


                        }
                        else {

                            Toast.makeText(context, "please check internet", Toast.LENGTH_SHORT).show();


                        }
                        classtipTopics.get(pos).downloadID=-1;
                        classtipTopics.get(pos).isdownloading=false;
                        classtipTopics.get(pos).isresumed=false;
                        classtipTopics.get(pos).ispaused=false;
                        notifyItemChanged(pos);

                    }
                });


        classtipTopics.get(pos).downloadID=downloadId;
        classtipTopics.get(pos).isdownloading=true;
        notifyItemChanged(pos);


    }

     class DownloadImage extends AsyncTask<String,String,File> {

        String url="";
        String path="";

        ProgressDialog progressDialog=new ProgressDialog(context);


         public DownloadImage(String url, String path) {
             this.url = url;
             this.path = path;
         }

         @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.show();

        }
        @Override
        protected File doInBackground(String... URL) {
            String imageURL = url;
            Bitmap bitmap = null;

            File f=null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);

                int w = (int) context.getResources().getDimension(R.dimen.dimen_150dp);

                Bitmap bitmap_converted = DownloadUtils.getResizedBitmap(bitmap, w, w);


                 f = new File(path);
                f.createNewFile();

//Convert bitmap to byte array
              //  Bitmap bitmap = bitmap_converted;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap_converted.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return f;
        }
        @Override
        protected void onPostExecute(File result) {

             progressDialog.dismiss();

            try {
                // Set the bitmap into ImageView


            }catch (Exception e)
            {

            }

        }
    }








}
