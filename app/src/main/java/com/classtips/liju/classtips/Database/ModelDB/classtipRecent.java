package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by User on 9/5/2018.
 */
@Entity(tableName="classtipRecent")
public class classtipRecent {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="id")
    public int id;

    @ColumnInfo(name="videoName")
    public String videoName;

    @ColumnInfo(name="videoType")
    public String videoType;

    @Nullable
    @ColumnInfo(name="topicId")
    public String topicId;

    @ColumnInfo(name="url")
    public String url;

    @Nullable
    @ColumnInfo(name="chapterId")
    public String chapterId;
}
