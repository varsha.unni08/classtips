package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by l on 30-08-2019.
 */

public class QuestionYearViewHolder extends RecyclerView.ViewHolder  {
    public ImageView image_product;
    public TextView text_topic_name,text_watch,text_answer_img;

    public QuestionYearViewHolder(View itemView) {
        super(itemView);
        image_product=(ImageView)itemView.findViewById(R.id.image_product);
//        image_product=(ImageView)itemView.findViewById(R.id.myZoomageView);
        text_watch=(TextView)itemView.findViewById(R.id.text_watch);
        text_topic_name=(TextView)itemView.findViewById(R.id.txt_question_name);
        text_answer_img=(TextView)itemView.findViewById(R.id.text_answer_img);

    }


}
