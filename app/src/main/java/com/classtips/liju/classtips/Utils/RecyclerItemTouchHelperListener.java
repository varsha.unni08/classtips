package com.classtips.liju.classtips.Utils;

import android.support.v7.widget.RecyclerView;

/**
 * Created by User on 10/3/2018.
 */

public interface RecyclerItemTouchHelperListener {

    void onSwiped(RecyclerView.ViewHolder viewHolder,int direction,int position);
}
