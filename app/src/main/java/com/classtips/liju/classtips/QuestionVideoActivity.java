package com.classtips.liju.classtips;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.classtips.liju.classtips.Adapter.MoreDetailedVideoAdapter;
import com.classtips.liju.classtips.Downloading.EncryptDecryptUtils;
import com.classtips.liju.classtips.Downloading.FileUtils;
import com.classtips.liju.classtips.Downloading.Utils;
import com.classtips.liju.classtips.Model.MoreDetailVideo;
import com.classtips.liju.classtips.Model.Video;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;

import java.io.File;
import java.io.IOException;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class QuestionVideoActivity extends AppCompatActivity {
    VideoView videoView;
    Button btn_question;
    String key,topiId;
    IClassTipAPI mService;
    RecyclerView more_details_video;

    ProgressBar progressBar = null;
    private int position = 0;
    private MediaController mediaController;
    CompositeDisposable compositeDisposable=new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_video);

        more_details_video = findViewById(R.id.recycler_more_details_video);
        more_details_video.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        more_details_video.setHasFixedSize(true);

        key=getIntent().getStringExtra("key");
        if (key==null){
            key="0";
        }

        mService= Common.getAPI();
        progressBar = findViewById(R.id.progressbar);
        videoView = findViewById(R.id.videoView);
        if (mediaController == null) {
            mediaController = new MediaController(QuestionVideoActivity.this);

            // Set the videoView that acts as the anchor for the MediaController.
            mediaController.setAnchorView(videoView);


            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);
        }

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {


                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        mediaController.setAlpha(0.5f);
//                        mediaController.setBackgroundColor(getResources().getColor(R.color.red));
                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(videoView);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
        btn_question= findViewById(R.id.btn_question) ;

        btn_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        playVideo();

        if (this.getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE)
            lanscapeConfigurationListener();

    }

    private void lanscapeConfigurationListener() {
        RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        btn_question.setVisibility(View.GONE);
        more_details_video.setVisibility(View.GONE);

        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    // TODO: The system bars are visible. Make any desired
                    // adjustments to your UI, such as showing the action bar or
                    // other navigational controls.

                    decorView.setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_IMMERSIVE
                                    // Set the content to appear under the system bars so that the
                                    // content doesn't resize when the system bars hide and show.
                                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    // Hide the nav bar and status bar
//                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN);

                    Handler h = new Handler();

                    h.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            decorView.setSystemUiVisibility(
                                    View.SYSTEM_UI_FLAG_IMMERSIVE
                                            // Set the content to appear under the system bars so that the
                                            // content doesn't resize when the system bars hide and show.
                                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                            // Hide the nav bar and status bar
                                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
                        }
                    }, 5000);


                } else {
                    // TODO: The system bars are NOT visible. Make any desired
                    // adjustments to your UI, such as hiding the action bar or
                    // other navigational controls.
                }
            }
        });
    }

    private void playVideo() {


        if (key=="0") {
            if (isConnectingToInternet()) {
                try{
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        System.out.println("videoView = " + Common.BASE_URL+"/"+Common.currentClasstipSubject.subject_name+"/"+Common.currentClasstipQuestion.exam_name+"/"+ Common.currentClasstipQuestion.question_name);
                        videoView.setVideoPath(Common.currentClasstipQuestion.vimeo_url);
                    }else
                        videoView.setVideoPath(Common.BASE_URLM+"/"+Common.currentClasstipSubject.subject_name+"/"+Common.currentClasstipQuestion.exam_name+"/"+ Common.currentClasstipQuestion.question_name);

                }catch (NullPointerException e){
                    e.printStackTrace();
                   }

                try{
                    topiId=Common.currentTopic.topic_id;
                }catch (Exception e){
                    topiId=Common.currentClasstipTopic.topic_id;
                }

                getMoreDetailedVieos(topiId);

            } else {
                try{
//                    String url = Common.currentClasstipQuestion.question_video;
//                    String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                    String downloadFileName = Common.currentClasstipQuestion.question_name;;
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                    if (downloadFileName.indexOf(".") > 0)
                        downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                    Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                    try {
                        playAudio(FileUtils.getTempFileDescriptor(this, decrypt(ur, uri)));
                    } catch (IOException |NullPointerException e) {
                        Toast.makeText(this, "You yet havent download this video", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }

            }

        }
        else{
            if (isConnectingToInternet())
            {
                try {
                    videoView.setVideoPath(Common.currentQuestionYear.question_video);

                }catch (NullPointerException e){
                    videoView.setVideoPath(Common.currentClasstipQuestionYear.questionVideo);

                }
            }
            else
            {
                try{
                    String url = Common.currentClasstipQuestionYear.questionVideo;
                    String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);

                    if (downloadFileName.indexOf(".") > 0)
                        downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                    Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                    try {
                        playAudio(FileUtils.getTempFileDescriptor(this, decrypt(ur, uri)));
                    } catch (IOException |NullPointerException e) {
                        Toast.makeText(this, "You yet havent download this video", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }



                }catch (NullPointerException e){
                    String url = Common.currentQuestionYear.question_video;
                    String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                    if (downloadFileName.indexOf(".") > 0)
                        downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                    Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                    try {
                        playAudio(FileUtils.getTempFileDescriptor(this, decrypt(ur, uri)));
                    } catch (IOException |NullPointerException ex) {
                        Toast.makeText(this, "You yet havent download this video", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }

            }
        }

        videoView.start();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) videoView.getLayoutParams();
        final View decorView = getWindow().getDecorView();
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            lanscapeConfigurationListener();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            //Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
            more_details_video.setVisibility(View.VISIBLE);
            btn_question.setVisibility(View.VISIBLE);
//            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    } else {
                    }
                }
            });

            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            |View.SYSTEM_UI_FLAG_VISIBLE);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);
        }
        videoView.setLayoutParams(params);


    }

    private void getMoreDetailedVieos(String topic_id) {
        compositeDisposable.add(mService.getMoreVideos(topic_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Video>>() {
                    @Override
                    public void accept(List<Video> videos) throws Exception {
                        displayMoreDetailedList(videos);
                    }
                }));
    }

    public void onClickCalled(String url) {
        videoView.stopPlayback();
        videoView.setVideoPath(url);
        progressBar.setVisibility(View.VISIBLE);
        btn_question.setVisibility(View.VISIBLE);
    }

    private void displayMoreDetailedList(List<Video> moreDetailVideos) {
        MoreDetailedVideoAdapter adapter=new MoreDetailedVideoAdapter(this,moreDetailVideos);
        more_details_video.setAdapter(adapter);
    }

    private byte[] decrypt(Uri ur, Uri uri) {
        try {
//            Toast.makeText(this, "decrypting", Toast.LENGTH_SHORT).show();
            byte[] fileData = FileUtils.readFile(String.valueOf(ur));
            byte[] decryptedBytes = EncryptDecryptUtils.decode(EncryptDecryptUtils.getInstance(this).getSecretKey(), fileData);
//            FileUtils.saveFile(decryptedBytes, String.valueOf(uri));
            return decryptedBytes;
        } catch (Exception e) {
        }
        return null;
    }



    private void playAudio(File fileDescriptor) {
        if (null == fileDescriptor) {
            return;
        }
        play(fileDescriptor);
    }
    public void play(final File fileDescriptor) {

        try {
            Uri video = Uri.parse(String.valueOf(fileDescriptor));
            Toast.makeText(QuestionVideoActivity.this, String.valueOf(fileDescriptor), Toast.LENGTH_SHORT).show();
            videoView.setVideoURI(video);
            videoView.start();

        } catch (Exception e) {
        }
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onPause() {
        Log.d("TAG", "onPause called");
        super.onPause();
        position = videoView.getCurrentPosition(); //stopPosition is an int
        videoView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("TAG", "onResume called");
        videoView.seekTo(position);
        videoView.start(); //Or use resume() if it doesn't work. I'm not sure
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
