package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/12/2018.
 */
@Entity(tableName = "classtipCourse")
public class classtipCourse {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="id")
    public String id;

    @ColumnInfo(name="courseName")
    public String courseName;

    @ColumnInfo(name="sylId")
    public String sylId;

    @ColumnInfo(name="standrdId")
    public String standrdId;
}
