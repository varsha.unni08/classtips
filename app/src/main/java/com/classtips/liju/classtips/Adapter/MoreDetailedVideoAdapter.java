package com.classtips.liju.classtips.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.MoreDetailVideo;
import com.classtips.liju.classtips.Model.Video;
import com.classtips.liju.classtips.QuestionVideoActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MoreDetailedVideoAdapter extends RecyclerView.Adapter<MoreDetailedVideoViewHolder> {

    String topicName;
    Context context;
    List<Video> detailVideoList;

    public MoreDetailedVideoAdapter(Context context, List<Video> detailVideoList) {
        this.context = context;
        this.detailVideoList = detailVideoList;
    }

    @NonNull
    @Override
    public MoreDetailedVideoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.video_item_layout,null);
        return new MoreDetailedVideoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MoreDetailedVideoViewHolder holder, final int position) {

        try{
            topicName= Common.currentTopic.topic_name;
        }catch (Exception e)
        {
            topicName=Common.currentClasstipTopic.topic_name;
        }

        //Load image
//        Picasso.with(context)
//                .load(detailVideoList.get(position).video_img)
//                .into(holder.image_video);

        holder.txt_description.setText(topicName);

        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                ((QuestionVideoActivity) context).onClickCalled(detailVideoList.get(position).url);
                holder.cardView.setVisibility(View.GONE);
//                Toast.makeText(context, detailVideoList.get(position).youtube_id, Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return detailVideoList.size();
    }
}
