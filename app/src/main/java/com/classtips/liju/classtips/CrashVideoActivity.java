package com.classtips.liju.classtips;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;
import com.classtips.liju.classtips.Downloading.EncryptDecryptUtils;
import com.classtips.liju.classtips.Downloading.FileUtils;
import com.classtips.liju.classtips.Downloading.Utils;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Model.Video;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrashVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{
    VideoView videoView;
    Button btn_question;
    String key;
    IClassTipAPI mService;
    String phone,subject_id,chapter_id,classmode_id;
    private final String API_KEY="AIzaSyCb4JlXATybV1BL5mYCTY3Cd_Rp1quwD2Q";
    private final String VIDEO_CODE="HXG9WcmySME";
    YouTubePlayerView youtubePlayer;

    private int position = 0;
    private MediaController mediaController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_video);
        mService= Common.getAPI();
        videoView = (VideoView)findViewById(R.id.videoView);
        youtubePlayer=(YouTubePlayerView)findViewById(R.id.youtubePlayer);
        if (mediaController == null) {
            mediaController = new MediaController(CrashVideoActivity.this);

            // Set the videoView that acts as the anchor for the MediaController.
            mediaController.setAnchorView(videoView);


            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);
        }

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {


                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });
        btn_question=(Button)findViewById(R.id.btn_question) ;

        btn_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(CrashVideoActivity.this, ChapterActivity.class));
                finish();
            }
        });

        if (isConnectingToInternet()) {

            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();

            try {
                phone=Common.currentUser.getPhone();
            }catch (NullPointerException e){
                phone=pref.getString("phone", null);
            }
            try {
                chapter_id=Common.currentChapter.chapter_id;
            }catch (NullPointerException e){
                chapter_id=Common.currentClasstipChapter.chapter_id;
            }
            try {
                subject_id=Common.currentSubject.subject_id;
            }catch (NullPointerException e){
                subject_id=Common.currentClasstipSubject.subject_id;
            }
            try {
                classmode_id=Common.currentClassmode.class_mode_id;
            }catch (NullPointerException e){
                classmode_id=Common.currentClasstipClassMode.class_mode_id;
            }

            editor.commit();

            mService.checkPurchaseExists(phone,subject_id)
                    .enqueue(new Callback<CheckPurchaseResponse>() {
                        @Override
                        public void onResponse(Call<CheckPurchaseResponse> call, Response<CheckPurchaseResponse> response) {
                            CheckPurchaseResponse purchaseResponse = response.body();
                            if (purchaseResponse.isExists()) {
                                //Toast.makeText(VideoHomeActivity.this, "exists", Toast.LENGTH_SHORT).show();

                                mService.getCrashVideos(chapter_id,classmode_id)
                                        .enqueue(new Callback<Video>() {
                                            @Override
                                            public void onResponse(Call<Video> call, Response<Video> response) {
                                                final Video video = response.body();
                                                //Common.currentVideo=response.body();
//                                                    videoView.setVideoPath(video.getUrl());
//                                                    videoView.start();
                                                videoView.setVisibility(View.INVISIBLE);
                                                youtubePlayer.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
                                                    @Override
                                                    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                                                        if (!b){
                                                            youTubePlayer.loadVideo(VIDEO_CODE);
                                                            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                                                        }
                                                    }

                                                    @Override
                                                    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                                                        Toast.makeText(CrashVideoActivity.this, youTubeInitializationResult.toString(), Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            }

                                            @Override
                                            public void onFailure(Call<Video> call, Throwable t) {


                                            }
                                        });
                            } else {
                                // Toast.makeText(VideoHomeActivity.this, "not", Toast.LENGTH_SHORT).show();
                                mService.getCrashVideos(chapter_id,classmode_id)
                                        .enqueue(new Callback<Video>() {
                                            @Override
                                            public void onResponse(Call<Video> call, Response<Video> response) {
                                                Video video = response.body();
                                                if (video.getVideoType().equals("P")) {
                                                    Toast.makeText(CrashVideoActivity.this, "You need to purchase", Toast.LENGTH_SHORT).show();
                                                    showUpdateDialogue();
                                                } else {

                                                    videoView.setVideoPath(video.getUrl());
                                                    videoView.start();
                                                    //Toast.makeText(VideoHomeActivity.this, String.valueOf(video.getUrl()), Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<Video> call, Throwable t) {

                                            }
                                        });
                            }
                        }

                        @Override
                        public void onFailure(Call<CheckPurchaseResponse> call, Throwable t) {

                        }
                    });

        }
        else
        {
            //Common.recentRepository.getRecentVideosById(Integer.parseInt(Common.currentClasstipTopic.id));

            try {
                chapter_id=Common.currentClasstipChapter.chapter_id;
            }catch (NullPointerException e){
                chapter_id=Common.currentChapter.chapter_id;
            }
            try {
                classmode_id=Common.currentClasstipClassMode.class_mode_id;
            }catch (NullPointerException e){
                classmode_id=Common.currentClassmode.class_mode_id;
            }

            classtipRecent video=new classtipRecent();
            // Toast.makeText(this,new Gson().toJson(Common.recentRepository.getRecentVideosById(Integer.parseInt(Common.currentTopic.topic_id))), Toast.LENGTH_SHORT).show();
            //String recs = new Gson().toJson(Common.recentRepository.getRecentVideosById(Integer.parseInt(Common.currentTopic.topic_id)));

            try {
                JSONArray req = new JSONArray(new Gson().toJson(Common.recentRepository.getRecentVideosById(Integer.parseInt(chapter_id))));
                for (int i = 0; i < req.length(); ++i) {
                    JSONObject rec = req.getJSONObject(i);
                    int id = rec.getInt("id");
                    String url = rec.getString("url");
                    String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                    if (downloadFileName.indexOf(".") > 0)
                        downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                    Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                            + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                    try {
                        playAudio(FileUtils.getTempFileDescriptor(this, decrypt(ur, uri)));
                    } catch (IOException|NullPointerException e) {
                        Toast.makeText(this, "You yet havent download this video", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    Log.e("test", String.valueOf(ur));

                }
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {



    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {




    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) videoView.getLayoutParams();


        final View decorView = getWindow().getDecorView();
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            btn_question.setVisibility(View.GONE);
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        // TODO: The system bars are visible. Make any desired
                        // adjustments to your UI, such as showing the action bar or
                        // other navigational controls.
                        decorView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_IMMERSIVE
                                        // Set the content to appear under the system bars so that the
                                        // content doesn't resize when the system bars hide and show.
                                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                        // Hide the nav bar and status bar
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
                    } else {
                        // TODO: The system bars are NOT visible. Make any desired
                        // adjustments to your UI, such as hiding the action bar or
                        // other navigational controls.
                    }
                }
            });
//            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);


            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            // Set the content to appear under the system bars so that the
                            // content doesn't resize when the system bars hide and show.
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            // Hide the nav bar and status bar
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            //Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();

            btn_question.setVisibility(View.VISIBLE);
//            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    } else {
                    }
                }
            });

            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            |View.SYSTEM_UI_FLAG_VISIBLE);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);
        }
        videoView.setLayoutParams(params);


    }

    private byte[] decrypt(Uri ur, Uri uri) {
        try {
//            Toast.makeText(this, "decrypting", Toast.LENGTH_SHORT).show();
            byte[] fileData = FileUtils.readFile(String.valueOf(ur));
            byte[] decryptedBytes = EncryptDecryptUtils.decode(EncryptDecryptUtils.getInstance(this).getSecretKey(), fileData);
//            FileUtils.saveFile(decryptedBytes, String.valueOf(uri));
            return decryptedBytes;
        } catch (Exception e) {
        }
        return null;
    }



    private void playAudio(File fileDescriptor) {
        if (null == fileDescriptor) {
            return;
        }
        play(fileDescriptor);
    }
    public void play(final File fileDescriptor) {

        try {
            Uri video = Uri.parse(String.valueOf(fileDescriptor));
            Toast.makeText(CrashVideoActivity.this, String.valueOf(fileDescriptor), Toast.LENGTH_SHORT).show();
            videoView.setVideoURI(video);
            videoView.start();

        } catch (Exception e) {
        }
    }

    private void showUpdateDialogue() {
        final AlertDialog.Builder builder=new AlertDialog.Builder(CrashVideoActivity.this);
        builder.setTitle("Purchae Subject");
        View itemView= LayoutInflater.from(CrashVideoActivity.this)
                .inflate(R.layout.purchase_layout,null);

        builder.setView(itemView);

        builder.setMessage("You have to purchase subject for accessing this video class.Do you want to purchase?")
                .setCancelable(false)
                .setPositiveButton("PURCHASE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent=new Intent(CrashVideoActivity.this,PurchaseActivity.class);
                        //intent.putExtra("key","2");
                        startActivity(intent);
                        finish();
                    }
                }) .setNegativeButton("Later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onBackPressed() {
            startActivity(new Intent(CrashVideoActivity.this, ChapterActivity.class));
        finish();
    }
}
