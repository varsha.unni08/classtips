package com.classtips.liju.classtips.Adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.classtips.liju.classtips.Model.Exams;
import com.classtips.liju.classtips.R;

import java.util.List;

/**
 * Created by User on 9/13/2018.
 */

public class ExamAdapter extends ArrayAdapter<String> {
    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<Exams> exams;
    private final int mResource;

    public ExamAdapter(@NonNull Context context, @LayoutRes int resource,
                              @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        exams = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView examName = (TextView) view.findViewById(R.id.txt_exam_name);
        TextView id = (TextView) view.findViewById(R.id.txt_id);

        Exams examsData = exams.get(position);

        examName.setText(examsData.exam_name);
        id.setText(examsData.exam_id);
        //Toast.makeText(mContext, examsData.exam_id, Toast.LENGTH_SHORT).show();

        return view;
    }
}
