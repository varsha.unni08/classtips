package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.QuestionViewHolder;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.QuestionVideoActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 9/27/2018.
 */

public class QuestionAdapterDB extends RecyclerView.Adapter<QuestionViewHolder> {
    Context context;
    List<classtipQuestion> classtipQuestions;

    public QuestionAdapterDB(Context context, List<classtipQuestion> classtipQuestions) {
        this.context = context;
        this.classtipQuestions = classtipQuestions;
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.question_item_layout,null);
        return new QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, final int position) {

        Picasso.with(context)
                .load(classtipQuestions.get(position).question_img)
                .into(holder.image_product);

        holder.text_watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.currentClasstipQuestion=classtipQuestions.get(position);
                Common.currentQuestion=null;

                context.startActivity(new Intent(context,QuestionVideoActivity.class));
            }
        });

        holder.text_answer_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder answerView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.question_answer_layout, null);
                ImageView img_answer;
                img_answer=view.findViewById(R.id.img_answer);

                try{
                    Picasso.with(context)
                            .load(classtipQuestions.get(position).answer_img)
                            .into(img_answer);
                }catch (Exception e){

                }

                answerView.setView(view);
                answerView.show();
//
            }
        });

        holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder zoomView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.zooming_layout, null);
//                ImageView image_product;
                PhotoView photoView;
                photoView=view.findViewById(R.id.photo_view);
                                Picasso.with(context)
                        .load(classtipQuestions.get(position).question_img)
                        .into(photoView);
                zoomView.setView(view);

                zoomView.show();
//                Intent intent=new Intent(context,ZoomingActivity.class);
//                intent.putExtra("name",questions.get(position).question_img);
//                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return classtipQuestions.size();
    }

    public void setQuestionList(List<classtipQuestion> classtipQuestions) {
        this.classtipQuestions = classtipQuestions;
        notifyDataSetChanged();
    }
}
