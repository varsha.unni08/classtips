package com.classtips.liju.classtips;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.Subject2Adapter;
import com.classtips.liju.classtips.Database.AdapterDB.SubjectAdapterDB;
import com.classtips.liju.classtips.Database.AdapterDB.SubjectAdapterDB2;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.config.AppPreferences;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.classtips.liju.classtips.config.ApplicationConstants.APPLICATION_DATA;
import static com.classtips.liju.classtips.config.ApplicationConstants.COURSE_ID;
import static com.classtips.liju.classtips.config.ApplicationConstants.PHONE;

public class SubjectActivity extends AppCompatActivity {
    IClassTipAPI mService;
    RecyclerView sub_menu;
    ProgressBar progressBar = null;
    SubjectAdapterDB2 subjectAdapterDB2;
    AppPreferences appPreferences;
    String courseId;

    CompositeDisposable compositeDisposable=new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        appPreferences = AppPreferences.getInstance(SubjectActivity.this, APPLICATION_DATA);
        mService= Common.getAPI();
        sub_menu=(RecyclerView)findViewById(R.id.sub_menu);
        sub_menu.setLayoutManager(new GridLayoutManager(this,2));
        sub_menu.setHasFixedSize(true);

        subjectAdapterDB2 = new SubjectAdapterDB2(SubjectActivity.this, new ArrayList<classtipSubject>());
        sub_menu.setAdapter(subjectAdapterDB2);

        progressBar = (ProgressBar) findViewById(R.id.progressbar);


        compositeDisposable.add(Common.userRepository.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipUser>>() {
                    @Override
                    public void accept(List<classtipUser> classtipUsers) throws Exception {
                        Log.d("classtip234", new Gson().toJson(classtipUsers));
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                courseId = rec.getString("courseId");
                            }
                            if (isConnectingToInternet()) {
                                progressBar.setVisibility(View.VISIBLE);
                                    getSubject(courseId);
                            }
                                getSubjectDB(courseId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    private void getSubjectDB(String course) {
        compositeDisposable.add(Common.subjectRepository.getSubjectsById(Integer.parseInt(course))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipSubject>>() {
                    @Override
                    public void accept(List<classtipSubject> classtipSubjects) throws Exception {
                        progressBar.setVisibility(View.GONE);
                        subjectAdapterDB2.setSubjectList(classtipSubjects);
                    }
                }));
    }

    private void getSubject(final String course_id) {

        final   AppPreferences appPreferences = AppPreferences.getInstance(SubjectActivity.this, APPLICATION_DATA);



        compositeDisposable.add(mService.getSubjectswithpurchase(course_id,appPreferences.getString(PHONE))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onExceptionResumeNext(new Observable<List<classtipSubject>>() {
                    @Override
                    protected void subscribeActual(Observer<? super List<classtipSubject>> observer) {
                        Log.d("exception123","Subjectexception");
                        progressBar.setVisibility(View.GONE);
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(SubjectActivity.this, "NetConnection is tooo Slow!!!!", Toast.LENGTH_SHORT).show();
                    }
                })
                .subscribe(new Consumer<List<classtipSubject>>() {
                    @Override
                    public void accept(List<classtipSubject> subjects) throws Exception {
                        subjectAdapterDB2.setSubjectList(subjects);
                        progressBar.setVisibility(View.GONE);
                    }
                }));
    }



    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
