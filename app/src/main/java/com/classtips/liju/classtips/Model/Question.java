package com.classtips.liju.classtips.Model;

/**
 * Created by User on 8/22/2018.
 */

public class Question {
    public int question_id;
    public String question_img;
    public String question_video;
    public String exam_name;
    public String question_type;
    public String group_no;
    public String order_no;
    public String question_flag;
    public String updtn_flg;
    public int exam_id;
    public int topic_id;
    public int course_id;
    public String answer_img;
    public String question_name;
    public String vimeo_url;
    public String question_text;

}
