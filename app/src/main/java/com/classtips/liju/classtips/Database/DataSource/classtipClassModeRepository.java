package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/24/2018.
 */

public class classtipClassModeRepository implements IclasstipClassModeDataSource {

    private IclasstipClassModeDataSource iclasstipClassModeDataSource;
    private static classtipClassModeRepository instance;

    public classtipClassModeRepository(IclasstipClassModeDataSource iclasstipClassModeDataSource) {
        this.iclasstipClassModeDataSource = iclasstipClassModeDataSource;
    }

    public static classtipClassModeRepository getInstance(IclasstipClassModeDataSource iclasstipClassModeDataSource){
        if (instance==null)
            instance=new classtipClassModeRepository(iclasstipClassModeDataSource);
            return instance;

    }

    @Override
    public Flowable<List<classtipClassMode>> getClassModes() {
        return iclasstipClassModeDataSource.getClassModes();
    }

    @Override
    public int isExist(int classModeId) {
        return iclasstipClassModeDataSource.isExist(classModeId);
    }

    @Override
    public int countClassModes() {
        return iclasstipClassModeDataSource.countClassModes();
    }

    @Override
    public void emptyClassModes() {
        iclasstipClassModeDataSource.emptyClassModes();
    }

    @Override
    public void insertClassModes(classtipClassMode... classtipClassModes) {
        iclasstipClassModeDataSource.insertClassModes(classtipClassModes);
    }

    @Override
    public void updateClassmodes(classtipClassMode... classtipClassModes) {
        iclasstipClassModeDataSource.updateClassmodes(classtipClassModes);
    }
}
