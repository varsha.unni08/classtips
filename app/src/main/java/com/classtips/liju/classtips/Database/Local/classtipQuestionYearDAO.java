package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/27/2018.
 */
@Dao
public interface classtipQuestionYearDAO {

    @Query("SELECT * FROM classtipQuestionYear")
    Flowable<List<classtipQuestionYear>> getQuestions();

    @Query("SELECT * FROM classtipQuestionYear where examId=:examId")
    Flowable<List<classtipQuestionYear>> getQuestionsById(int examId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipQuestionYear where id=:questionId)")
    int isExist(int questionId);

    @Query("SELECT COUNT(*) FROM classtipQuestionYear")
    int countQuestions();

    @Insert
    void insertQuestions(classtipQuestionYear...classtipQuestions);

    @Update
    void updateQuestions(classtipQuestionYear...classtipQuestionYears);
}
