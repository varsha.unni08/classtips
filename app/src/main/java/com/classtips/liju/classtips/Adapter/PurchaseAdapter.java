package com.classtips.liju.classtips.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.PurchaseActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by User on 8/30/2018.
 */

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseViewHolder> {
    Context context;
    List<classtipSubject> subjects;
    String courseId,phone;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public PurchaseAdapter(Context context, List<classtipSubject> subjects) {
        this.context = context;
        this.subjects = subjects;
    }

    @NonNull
    @Override
    public PurchaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.purchase_item_layout,null);
        return new PurchaseViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PurchaseViewHolder holder, final int position) {
        Picasso.with(context)
                .load(subjects.get(position).link)
                .into(holder.image_purchase);
        holder.txt_purchase.setText(subjects.get(position).subject_name);
        holder.text_price.setText(new StringBuilder("₹").append(subjects.get(position).prize));
        if (Common.purchaseRepository.isExist(Integer.parseInt(subjects.get(position).subject_id))==1){
            holder.cardView.setCardBackgroundColor(Color.WHITE);
            holder.btn_purchase.setText("Added To Cart");
            holder.btn_purchase.setEnabled(false);
        }
        holder.btn_purchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.currentClasstipSubject= subjects.get(position);
                compositeDisposable.add(Common.userRepository.getUsers()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<List<classtipUser>>() {
                            @Override
                            public void accept(List<classtipUser> classtipUsers) throws Exception {
                                Log.d("classtip234", new Gson().toJson(classtipUsers));
                                try {
                                    JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                                    for (int i = 0; i < req.length(); ++i) {
                                        JSONObject rec = req.getJSONObject(i);
                                        phone = rec.getString("phone");
                                        courseId = rec.getString("courseId");
                                    }
                                    if (Common.purchaseRepository.isExist(Integer.parseInt(subjects.get(position).subject_id))!=1)
                                    {
                                        classtipPurchase purchase=new classtipPurchase();
                                        purchase.courseId= Integer.parseInt(courseId);
                                        purchase.subId= Integer.parseInt(Common.currentClasstipSubject.subject_id);
                                        purchase.phone=phone;
                                        purchase.subjectName=Common.currentClasstipSubject.subject_name;
                                        purchase.price=Common.currentClasstipSubject.prize;

                                        Common.purchaseRepository.insertPurchases(purchase);
                                        Toast.makeText(context, new Gson().toJson(Common.purchaseRepository.countPurchases()), Toast.LENGTH_SHORT).show();
                                        Log.d("classtip23", new Gson().toJson(purchase));
                                        ((PurchaseActivity)context).resetGraph(context);
                                    }
                                    else
                                    {
                                        Toast.makeText(context, "This Item Already in Cart", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }));
            }
        });
        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }

    public void setSubjectList(List<classtipSubject> classtipSubjects) {
        this.subjects = classtipSubjects;
        notifyDataSetChanged();
    }
}
