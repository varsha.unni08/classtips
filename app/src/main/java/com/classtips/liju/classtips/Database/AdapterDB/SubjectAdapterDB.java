package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.SubjectViewHolder;
import com.classtips.liju.classtips.ChapterActivity;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.UpdateFlag;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 9/11/2018.
 */

public class SubjectAdapterDB extends RecyclerView.Adapter<SubjectViewHolder> {
    Context context;
    List<classtipSubject> classtipSubjects;
    String expiryDate, instlnDate, phone, course;
    IClassTipAPI mService;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SubjectAdapterDB(Context context, List<classtipSubject> classtipSubjects) {
        this.context = context;
        this.classtipSubjects = classtipSubjects;
    }

    @Override
    public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.subj_menu_layout, null);
        return new SubjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubjectViewHolder holder, final int position) {

        mService = Common.getAPI();
//Load image
        Picasso.with(context)
                .load(classtipSubjects.get(position).link)
                .into(holder.image_product);
        holder.txt_subjct_name.setText(classtipSubjects.get(position).subject_name
        );
        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {

                Common.currentClasstipSubject=classtipSubjects.get(position);

                compositeDisposable.add(Common.userRepository.getUsers()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<List<classtipUser>>() {
                            @Override
                            public void accept(List<classtipUser> classtipUsers) throws Exception {
                                Log.d("classtip234", new Gson().toJson(classtipUsers));
                                try {
                                    JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                                    for (int i = 0; i < req.length(); ++i) {
                                        JSONObject rec = req.getJSONObject(i);
                                        phone = rec.getString("phone");
                                        instlnDate = rec.getString("instlnDate");
                                        expiryDate = rec.getString("expiryDate");

                                        SimpleDateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                                        Date date1 = null;
                                        try {
                                            date1 = dateFormat.parse(instlnDate);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        Date date2 = null;
                                        try {
                                            date2 = dateFormat.parse(expiryDate);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                        Calendar cal1 = Calendar.getInstance();
                                        Calendar cal2 = Calendar.getInstance();
                                        cal1.setTime(date1);
                                        cal2.setTime(date2);

                                        if (cal1.compareTo(cal2) > 0) {
                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                            alertDialogBuilder.setTitle("Your app is expired/Contact Us");
                                            alertDialogBuilder.setIcon(R.mipmap.classtiplogo);
                                            alertDialogBuilder.setMessage("Email:classtiptution@gmail.com\nPhone:8089379353");
                                            alertDialogBuilder.setNeutralButton("Call us", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                                    callIntent.setData(Uri.parse("tel:8089379353"));

                                                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                        // TODO: Consider calling

                                                    }
                                                    context.startActivity(callIntent);
                                                }
                                            }).show();
                                            Toast.makeText(context, "Your app is expired", Toast.LENGTH_LONG).show();
                                            mService.updateExpiryFlag(phone)
                                                    .enqueue(new Callback<UpdateFlag>() {
                                                        @Override
                                                        public void onResponse(Call<UpdateFlag> call, Response<UpdateFlag> response) {
                                                            UpdateFlag updateFlag = response.body();
                                                            if (updateFlag.isExpiryflagupdated()) {
                                                                Toast.makeText(context, "flag updated", Toast.LENGTH_SHORT).show();
//                                        Common.userRepository.updateExpiryFlag("Y",phone);
                                                                mService.coppyUser(phone)
                                                                        .enqueue(new Callback<UpdateFlag>() {
                                                                            @Override
                                                                            public void onResponse(Call<UpdateFlag> call, Response<UpdateFlag> response) {
                                                                                UpdateFlag updateFlag = response.body();
                                                                                if (updateFlag.isCoppied()) {
                                                                                    Toast.makeText(context, "coppied", Toast.LENGTH_SHORT).show();
                                                                                    mService.updatePurchaseFlag(phone, course)
                                                                                            .enqueue(new Callback<UpdateFlag>() {
                                                                                                @Override
                                                                                                public void onResponse(Call<UpdateFlag> call, Response<UpdateFlag> response) {

                                                                                                }

                                                                                                @Override
                                                                                                public void onFailure(Call<UpdateFlag> call, Throwable t) {

                                                                                                }
                                                                                            });
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onFailure(Call<UpdateFlag> call, Throwable t) {

                                                                            }
                                                                        });
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<UpdateFlag> call, Throwable t) {

                                                        }
                                                    });
                                        } else if (cal1.compareTo(cal2) < 0) {
                                            Common.currentClasstipClassMode = null;
                                            Common.currentClassmode = null;
                                            Common.currentSubject = null;
//                Toast.makeText(context, Common.currentClasstipClassMode.classModeName, Toast.LENGTH_SHORT).show();
                                            Common.currentClasstipSubject = classtipSubjects.get(position);
                                            //Toast.makeText(context, Common.currentClasstipSubject.subjectName, Toast.LENGTH_SHORT).show();
                                            context.startActivity(new Intent(context, ChapterActivity.class));

                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }));


//                Toast.makeText(context, "date"+expiryDate+instlnDate, Toast.LENGTH_LONG).show();


            }
        });

    }

    @Override
    public int getItemCount() {
        return classtipSubjects.size();
    }

    public void setSubjectList(List<classtipSubject> classtipSubjects) {
        this.classtipSubjects = classtipSubjects;
        notifyDataSetChanged();
    }
}
