package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */

public interface IclasstipChapterDataSource {


    Flowable<List<classtipChapter>> getChapters();

    Flowable<List<classtipChapter>> getChaptersById(int subId);

    int isExist(int chapId);

    int countChapters();

    void  emptyChapters();

    void updateChapters(classtipChapter...classtipChapters);

    void insertChapters(classtipChapter...classtipChapters);
}
