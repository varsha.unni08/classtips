package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipChapterDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */

public class classtipChapterDataSource implements IclasstipChapterDataSource {

    private classtipChapterDAO chapterDAO;
    private static classtipChapterDataSource instance;

    public classtipChapterDataSource(classtipChapterDAO chapterDAO) {
        this.chapterDAO = chapterDAO;
    }

    public static classtipChapterDataSource getInstance(classtipChapterDAO chapterDAO) {
        if (instance==null)
            instance=new classtipChapterDataSource(chapterDAO);
        return instance;
    }

    @Override
    public Flowable<List<classtipChapter>> getChapters() {
        return chapterDAO.getChapters();
    }

    @Override
    public Flowable<List<classtipChapter>> getChaptersById(int subId) {
        return chapterDAO.getChaptersById(subId);
    }

    @Override
    public int isExist(int chapId) {
        return chapterDAO.isExist(chapId);
    }

    @Override
    public int countChapters() {
        return chapterDAO.countChapters();
    }

    @Override
    public void emptyChapters() {
        chapterDAO.emptyChapters();
    }

    @Override
    public void updateChapters(classtipChapter... classtipChapters) {
        chapterDAO.updateChapters(classtipChapters);
    }

    @Override
    public void insertChapters(classtipChapter... classtipChapters) {
        chapterDAO.insertChapters(classtipChapters);
    }
}
