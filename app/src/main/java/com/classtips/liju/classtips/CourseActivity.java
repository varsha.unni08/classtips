package com.classtips.liju.classtips;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.classtips.liju.classtips.Adapter.CourseAdapter;
import com.classtips.liju.classtips.Model.Course;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CourseActivity extends AppCompatActivity {
    IClassTipAPI mService;
    RecyclerView course_menu;

    CompositeDisposable compositeDisposable=new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);

        mService= Common.getAPI();
        course_menu=(RecyclerView)findViewById(R.id.course_menu);
        course_menu.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        course_menu.setHasFixedSize(true);

        loadCourseList();
    }

    private void loadCourseList() {
        compositeDisposable.add(mService.getCourses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Course>>() {
                    @Override
                    public void accept(List<Course> courses) throws Exception {
                        displayCourseList(courses);
                    }
                }));
    }

    private void displayCourseList(List<Course> courses) {
        CourseAdapter adapter=new CourseAdapter(this,courses);
        course_menu.setAdapter(adapter);
    }
}
