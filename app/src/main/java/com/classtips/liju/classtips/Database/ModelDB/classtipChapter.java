package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/7/2018.
 */
@Entity(tableName = "classtipChapter")
public class classtipChapter {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="id")
    public String chapter_id;

    @ColumnInfo(name="chapterName")
    public String chapter_name;

    @ColumnInfo(name="subId")
    public String sub_id;

    @ColumnInfo(name="priority")
    public String priority;

}
