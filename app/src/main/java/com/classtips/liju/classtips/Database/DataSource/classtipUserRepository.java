package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipUser;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/12/2018.
 */

public class classtipUserRepository implements IclasstipUserDataSource {

    private IclasstipUserDataSource iclasstipUserDataSource;
    private static classtipUserRepository instance;

    public classtipUserRepository(IclasstipUserDataSource iclasstipUserDataSource) {
        this.iclasstipUserDataSource = iclasstipUserDataSource;
    }

    public static classtipUserRepository getInstance(IclasstipUserDataSource iclasstipUserDataSource){
        if (instance==null)
            instance=new classtipUserRepository(iclasstipUserDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipUser>> getUsers() {
        return iclasstipUserDataSource.getUsers();
    }

    @Override
    public Flowable<List<classtipUser>> getUsersById(int courseId) {
        return iclasstipUserDataSource.getUsersById(courseId);
    }

    @Override
    public int isExist(int phone) {
        return iclasstipUserDataSource.isExist(phone);
    }

    @Override
    public int isUpdated(String flag, String phone) {
        return iclasstipUserDataSource.isUpdated(flag,phone);
    }

    @Override
    public void updateUser(String flag, String phone) {
        iclasstipUserDataSource.updateUser(flag, phone);
    }

    @Override
    public int countUsers() {
        return iclasstipUserDataSource.countUsers();
    }

    @Override
    public void insertUsers(classtipUser... classtipUsers) {
        iclasstipUserDataSource.insertUsers(classtipUsers);
    }

    @Override
    public void updateUserDate(String date, String phone) {
        iclasstipUserDataSource.updateUserDate(date, phone);
    }

    @Override
    public void updateExpiryFlag(String flag, String phone) {
        iclasstipUserDataSource.updateExpiryFlag(flag, phone);
    }
}
