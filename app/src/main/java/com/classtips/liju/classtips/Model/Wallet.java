package com.classtips.liju.classtips.Model;

/**
 * Created by User on 9/15/2018.
 */

public class Wallet {

    public String phone ;
    public String walletmoney;
    public String walletid ;

    public Wallet() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWalletmoney() {
        return walletmoney;
    }

    public void setWalletmoney(String walletmoney) {
        this.walletmoney = walletmoney;
    }

    public String getWalletid() {
        return walletid;
    }

    public void setWalletid(String walletid) {
        this.walletid = walletid;
    }
}
