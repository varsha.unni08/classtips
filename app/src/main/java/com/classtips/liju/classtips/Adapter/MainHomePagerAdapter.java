package com.classtips.liju.classtips.Adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.classtips.liju.classtips.Fragment.MainPagerFragment;

import java.util.List;

public class MainHomePagerAdapter extends FragmentPagerAdapter {



    public MainHomePagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int i) {

        Fragment fr=new MainPagerFragment();
        Bundle bundle=new Bundle();
        bundle.putSerializable("pos",i);
        fr.setArguments(bundle);

        return fr;
    }

    @Override
    public int getCount() {

//        if(offers.size()>4)
//        {
//            return 4;
//        }
//        else {
//
//
//            return offers.size();
//        }

return 2;

    }
}
