package com.classtips.liju.classtips.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.DownloadAdapter;
import com.classtips.liju.classtips.Adapter.PurchaseAdapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.DownloadList;
import com.classtips.liju.classtips.PurchaseActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class DownloadFragment extends Fragment {

    private Unbinder unbinder;

    IClassTipAPI mService;
    RecyclerView download_menu;
    ProgressBar progressBar = null;
    String course;
    DownloadAdapter downloadAdapter;
    CompositeDisposable compositeDisposable=new CompositeDisposable();
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("test","Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }
    }

    public DownloadFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_download, container, false);
        unbinder= ButterKnife.bind(this,view);

        mService= Common.getAPI();
        progressBar = (ProgressBar)view.findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        download_menu=(RecyclerView)view.findViewById(R.id.recycler_download);
        download_menu.setLayoutManager(new GridLayoutManager(getActivity(),2));
        download_menu.setHasFixedSize(true);
        downloadAdapter = new DownloadAdapter(requireActivity(), new ArrayList<DownloadList>());
        download_menu.setAdapter(downloadAdapter);

        if (isConnectingToInternet())
        {
            compositeDisposable.add(Common.userRepository.getUsers()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<classtipUser>>() {
                        @Override
                        public void accept(List<classtipUser> classtipUsers) throws Exception {
                            Log.d("classtip234", new Gson().toJson(classtipUsers));
                            try {
                                JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                                for (int i = 0; i < req.length(); ++i) {
                                    JSONObject rec = req.getJSONObject(i);
                                    course = rec.getString("courseId");
                                }
                                getDownloads(course);
                                isStoragePermissionGranted();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
        }
        else
            Toast.makeText(getActivity(), "Sorry You Are Offline", Toast.LENGTH_SHORT).show();
//

        return view;
    }


    private void getDownloads(String course_id) {
        compositeDisposable.add(mService.getDownloadList(course_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<DownloadList>>() {
                    @Override
                    public void accept(List<DownloadList> downloadLists) throws Exception {
                        downloadAdapter.setDownloadList(downloadLists);
                        progressBar.setVisibility(View.GONE);
                    }
                }));
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("test","Permission is granted");
                return true;
            } else {

                Log.v("test","Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("test","Permission is granted");
            return true;
        }
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
}
