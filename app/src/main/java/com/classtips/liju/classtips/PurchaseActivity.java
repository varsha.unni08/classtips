package com.classtips.liju.classtips;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.PurchaseAdapter;
import com.classtips.liju.classtips.Database.AdapterDB.SubjectAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;
import com.nex3z.notificationbadge.NotificationBadge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class PurchaseActivity extends AppCompatActivity {

    Button btn_qrcode;
    IClassTipAPI mService;
    RecyclerView sub_menu;
    CompositeDisposable compositeDisposable=new CompositeDisposable();
    String courseId,phone;
    NotificationBadge badge;
    ImageView cart_icon;
    PurchaseAdapter purchaseAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btn_qrcode=(Button)findViewById(R.id.btn_to_qrcode) ;
        btn_qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PurchaseActivity.this,QRcodeActivity.class);
                intent.putExtra("key","1");
                startActivity(intent);
                finish();
            }
        });
        mService= Common.getAPI();
        sub_menu=(RecyclerView)findViewById(R.id.recycler_purchase);
        sub_menu.setLayoutManager(new GridLayoutManager(this,2));
        sub_menu.setHasFixedSize(true);
        purchaseAdapter = new PurchaseAdapter(PurchaseActivity.this, new ArrayList<classtipSubject>());
        sub_menu.setAdapter(purchaseAdapter);

        if (isConnectingToInternet()){
            compositeDisposable.add(Common.userRepository.getUsers()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<classtipUser>>() {
                        @Override
                        public void accept(List<classtipUser> classtipUsers) throws Exception {
                            Log.d("classtip234", new Gson().toJson(classtipUsers));
                            try {
                                JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                                for (int i = 0; i < req.length(); ++i) {
                                    JSONObject rec = req.getJSONObject(i);
                                    courseId = rec.getString("courseId");
                                    phone = rec.getString("phone");
                                }
                                getSubject(courseId,phone);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }));
        }

        else
            Toast.makeText(this, "!Hey you are offline", Toast.LENGTH_LONG).show();
    }

    private void getSubject(String course_id,String phone) {
        compositeDisposable.add(mService.getSubjectPurchase(course_id,phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<classtipSubject>>() {
                    @Override
                    public void accept(List<classtipSubject> subjects) throws Exception {
                        purchaseAdapter.setSubjectList(subjects);
                    }
                }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_action_bar, menu);
        View view=menu.findItem(R.id.cart_menu).getActionView();
        badge=(NotificationBadge)view.findViewById(R.id.badge);
        cart_icon=(ImageView)view.findViewById(R.id.cart_icon) ;
        cart_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PurchaseActivity.this,CartActivity.class));
                finish();
            }
        });
        updateCount();
        return true;
    }

    private void updateCount() {
        if (badge==null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Common.purchaseRepository.countPurchases()==0)
                    badge.setVisibility(View.INVISIBLE);
                else
                    badge.setVisibility(View.VISIBLE);
                badge.setText(String.valueOf(Common.purchaseRepository.countPurchases()));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cart_menu) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void resetGraph(Context context) {

        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
