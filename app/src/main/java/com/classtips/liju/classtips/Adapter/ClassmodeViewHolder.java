package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by User on 8/10/2018.
 */

public class ClassmodeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView image_classmod;
    public TextView txt_clsmod_name;

    IItemClickListner itemClickListner;

    public void setItemClickListner(IItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public ClassmodeViewHolder(View itemView) {
        super(itemView);
        image_classmod=(ImageView)itemView.findViewById(R.id.image_clsmod);
        txt_clsmod_name=(TextView)itemView.findViewById(R.id.txt_clsmod_name);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListner.onClick(v);
    }
}
