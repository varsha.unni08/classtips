package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/12/2018.
 */
@Entity(tableName = "classtipUser")
public class classtipUser {

    @PrimaryKey
    @ColumnInfo(name="id")
    public int id;

    @ColumnInfo(name="phone")
    public String phone;

    @ColumnInfo(name="name")
    public String name;

    @ColumnInfo(name="birthDate")
    public String birthDate;

    @ColumnInfo(name="address")
    public String address;

    @ColumnInfo(name="courseId")
    public String courseId;

    @ColumnInfo(name="updtnFlag")
    public String updtnFlag;

    @ColumnInfo(name="expiryFlag")
    public String expiryFlag;

    @ColumnInfo(name="instlnDate")
    public String instlnDate;

    @ColumnInfo(name="expiryDate")
    public String expiryDate;
}
