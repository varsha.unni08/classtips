package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.SubjectViewHolder;
import com.classtips.liju.classtips.ChapterActivity;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.QuestionYearActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by User on 9/28/2018.
 */

public class SubjectAdapterDB2 extends RecyclerView.Adapter<SubjectViewHolder> {

    Context context;
    List<classtipSubject> classtipSubjects;
    String expiryDate,instlnDate;
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    public SubjectAdapterDB2(Context context, List<classtipSubject> classtipSubjects) {
        this.context = context;
        this.classtipSubjects = classtipSubjects;
    }

    @Override
    public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.subj_menu_layout,null);
        return new SubjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubjectViewHolder holder, final int position) {
        Picasso.with(context)
                .load(classtipSubjects.get(position).link)
                .into(holder.image_product);
        holder.txt_subjct_name.setText(classtipSubjects.get(position).subject_name
        );
        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                Common.currentClasstipSubject=classtipSubjects.get(position);
                Common.currentSubject=null;

                compositeDisposable.add(Common.userRepository.getUsers()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<List<classtipUser>>() {
                            @Override
                            public void accept(List<classtipUser> classtipUsers) throws Exception {
                                Log.d("classtip234", new Gson().toJson(classtipUsers));
                                try {
                                    JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                                    for (int i = 0; i < req.length(); ++i) {
                                        JSONObject rec = req.getJSONObject(i);
                                        instlnDate = rec.getString("instlnDate");
                                        expiryDate = rec.getString("expiryDate");
                                    }
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                                    Date date1 = null;
                                    try {
                                        date1 = dateFormat.parse(instlnDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    Date date2 = null;
                                    try {
                                        date2 = dateFormat.parse(expiryDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    if (date1.equals(date2)||date1.after(date2)){
                                        Toast.makeText(context, "Your app is expired", Toast.LENGTH_LONG).show();
                                    }else
                                    {
                                        try{
                                            if (!Common.currentClasstipClassMode.flag.equals("q2"))
                                            {
                                                context.startActivity(new Intent(context,ChapterActivity.class));
                                            }
                                            else{
                                                context.startActivity(new Intent(context, QuestionYearActivity.class));
                                            }
                                        }catch (NullPointerException e){
                                            if (!Common.currentClassmode.flag.equals("q2"))
                                            {
                                                context.startActivity(new Intent(context,ChapterActivity.class));
                                            }
                                            else{
                                                context.startActivity(new Intent(context, QuestionYearActivity.class));
                                            }
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }));

            }
        });
    }

    @Override
    public int getItemCount() {
        return classtipSubjects.size();
    }

    public void setSubjectList(List<classtipSubject> classtipSubjects) {
        this.classtipSubjects = classtipSubjects;
        notifyDataSetChanged();
    }
}
