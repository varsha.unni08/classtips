package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/15/2018.
 */

public interface IclasstipPurchaseDataSource {

    Flowable<List<classtipPurchase>> getPurchaseList();

    int isExist(int subId);

    int sumPrice();

    int countPurchases();

    void  emptyPurchases();

    void insertPurchases(classtipPurchase...classtipPurchases);

    void deletePurchases(classtipPurchase...classtipPurchases);
}
