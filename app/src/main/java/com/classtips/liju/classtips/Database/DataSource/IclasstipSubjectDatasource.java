package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */

public interface IclasstipSubjectDatasource {

    Flowable<List<classtipSubject>> getSubjects();

    Flowable<List<classtipSubject>> getSubjectsById(int subId);

    int isExist(int subId);

    int countSubjects();

    void  emptySubjects();

    void insertSubjects(classtipSubject...classtipSubjects);

    void updateSubjects(classtipSubject...classtipSubjects);

    void deleteSubjects(classtipSubject...classtipSubjects);
}
