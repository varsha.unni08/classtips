package com.classtips.liju.classtips.Downloading;

/**
 * Created by User on 9/20/2018.
 */

public class Utils {
    public static final String downloadDirectory = "classtip";
    public static final String mainUrl = "http://classtip.in/video/";
    public static final String downloadZipUrl = "http://classtip.in/video/videosnew.zip";
    public static final String downloadVideoUrl = "http://classtip.in/video/video.mp4";

    // Files
    public static final String DOWNLOAD_AUDIO_URL = "http://www.noiseaddicts.com/samples_1w72b820/272.mp3";
    public static final String FILE_NAME = "audio.mp3";
    public static final String TEMP_FILE_NAME = "temp";
    public static final String FILE_EXT = ".mp4";
    public static final String DIR_NAME = "Audio";
    public static final int OUTPUT_KEY_LENGTH = 256;

    // Algorithm
    public static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    public static final String KEY_SPEC_ALGORITHM = "AES";
    public static final String PROVIDER = "BC";

    public static final String SECRET_KEY = "SECRET_KEY";
}
