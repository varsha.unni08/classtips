package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.ChapterTopic;
import com.classtips.liju.classtips.Model.Topic;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.TopicActivity;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by User on 8/13/2018.
 */

public class ChapterAdapter extends RecyclerView.Adapter<ChapterViewHolder> {
    Context context;
    List<ChapterTopic> chapterTopics;

    public ChapterAdapter(Context context, List<ChapterTopic> chapterTopics) {
        this.context = context;
        this.chapterTopics = chapterTopics;
    }

    @NonNull
    @Override
    public ChapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView= LayoutInflater.from(context).inflate(R.layout.chapter_item_layout,null);
            return new ChapterViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ChapterViewHolder holder, final int position) {

        holder.txt_chapter_name.setText(chapterTopics.get(position).chapter_name);

    holder.setItemClickListner(new IItemClickListner() {
        @Override
        public void onClick(View v) {
            Toast.makeText(context, chapterTopics.get(position).sub_id, Toast.LENGTH_SHORT).show();
            //Start new activity
            Common.currentChapter= chapterTopics.get(position);
            Common.currentClasstipChapter=null;

            if (Common.chapterRepository.isExist(Integer.parseInt(chapterTopics.get(position).chapter_id))!=1)
            {
                Toast.makeText(context, "Chapter added", Toast.LENGTH_SHORT).show();
                classtipChapter chapter=new classtipChapter();
                chapter.chapter_id=chapterTopics.get(position).chapter_id;
                chapter.chapter_name=chapterTopics.get(position).chapter_name;
                chapter.sub_id=chapterTopics.get(position).sub_id;
                chapter.priority=chapterTopics.get(position).priority;

                Common.chapterRepository.insertChapters(chapter);
                Log.d("classtip23", new Gson().toJson(chapter));
                Toast.makeText(context, new Gson().toJson(Common.chapterRepository.countChapters()), Toast.LENGTH_SHORT).show();
            }

                context.startActivity(new Intent(context,TopicActivity.class));
//                ((Activity)context).finish();



        }
    });
    }

    @Override
    public int getItemCount() {

        return (chapterTopics != null ? chapterTopics.size() : 0);
    }
}
